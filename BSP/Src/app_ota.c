#include "app_ota.h"
#include "FreeRTOS.h"
#include "task.h"
#include "data.h"
#include "delay.h"

//md5
#include "digest_md5.h"


OTA_INFO ota_info;
UPDATE_INFO update_info;

osThreadId_t task_http_download_handle;
const osThreadAttr_t http_download_task_attributes = {
  .name = "http_download",
  .priority = (osPriority_t) osPriorityBelowNormal2,
  .stack_size = 2000
};


/*************************************************************
*	函数名称：	get_pack_sumer
*
*	函数功能：	将数据包累加
*
*	入口参数： *data：累加数据，len-数据长度
*
*	返回参数：	累加和
*
*	说明：		
*************************************************************/
uint16_t get_pack_sumer(uint8_t *data,uint16_t len)
{
	uint16_t sumer=0;
	int i;
	for(i=0;i<len;i++)
	{
		sumer += data[i];
	}
	return sumer;
}

/*************************************************************
*	函数名称：	check_date_sumer
*
*	函数功能：	将数据包累加校验
*
*	入口参数： data_addr，数据开始地址,data_len-数据包长度，check_sumer-正确的校验和
*
*	返回参数：	1-校验成功  ，0-校验失败
*
*	说明：		
*************************************************************/
uint8_t check_date_sumer(uint32_t data_addr,uint32_t data_len,uint16_t check_sumer)
{
	uint8_t  *flash_read_buf;
	uint32_t len = 0;
	uint16_t read_len =W25Q128FV_SUBSECTOR_SIZE;
	uint16_t sumer = 0;
	flash_read_buf=pvPortMalloc(W25Q128FV_SUBSECTOR_SIZE);
	while(len<data_len)
	{
		if(data_len-len<W25Q128FV_SUBSECTOR_SIZE)
		{
			read_len = data_len-len ;
		}
		
		BSP_W25Qx_Read(flash_read_buf,data_addr, read_len);//读取flash内数据
		sumer += get_pack_sumer(flash_read_buf,read_len);
		data_addr+=read_len;
		len += read_len;
	}
	vPortFree(flash_read_buf);
	LOG("sumer:0x%x\r\n",sumer);
	if(sumer == check_sumer)
	{
		system_parm.check_sumer[0]=sumer>>8;
        system_parm.check_sumer[1]=sumer;
		return 1;
		
	}
	else
	{
		return 0;
	}
}
/*************************************************************
*	函数名称：	check_date_sumer_flash
*
*	函数功能：	将数据包累加校验
*
*	入口参数： data_addr，外部flash数据开始地址,data_len-数据包长度，check_sumer-正确的校验和
*
*	返回参数：	1-校验成功  ，0-校验失败
*
*	说明：		
*************************************************************/
uint8_t check_date_sumer_flash(uint32_t data_addr,uint32_t data_len,uint16_t check_sumer)
{
	uint8_t  *flash_read_buf;
	uint32_t len = 0;
	uint16_t read_len =W25Q128FV_SUBSECTOR_SIZE;
	uint16_t sumer = 0;
	flash_read_buf=pvPortMalloc(W25Q128FV_SUBSECTOR_SIZE);
	if(flash_read_buf==NULL)
	{
		LOG("MALLOC ERROR\r\n");
		return 0;
	}
	while(len<data_len)
	{
		if(data_len-len<W25Q128FV_SUBSECTOR_SIZE)
		{
			read_len = data_len-len ;
		}
		
		BSP_W25Qx_Read(flash_read_buf, data_addr,read_len);//读取flash内数据
		sumer += get_pack_sumer(flash_read_buf,read_len);
		data_addr+=read_len;
		len += read_len;
	}
	vPortFree(flash_read_buf);
	LOG( "check sumer:0x%x\r\n",check_sumer);
	LOG("reality sumer:0x%x\r\n",sumer);
	if(sumer == check_sumer)
	{
		system_parm.check_sumer[0]=sumer>>8;
        system_parm.check_sumer[1]=sumer;
		return 1;
		
	}
	else
	{
		return 0;
	}
}

/*
************************************************************
*	函数名称：	ota_display
*
*	函数功能：	提示升级进度
*
*	入口参数：	bytes：已下载字节数
*				size：文件总大小
*
*	返回参数：	无
*
*	说明：		
************************************************************
*/
static void ota_display(unsigned int bytes, unsigned int size)
{

	if(bytes >= size)
	{
		LOG("Update progress %d/%d =100%%\r\n", size, size);
	}
	else
	{
		LOG("Update %d/%d =%0.2f%%\r\n", bytes, size, (float)bytes / size * 100);
	}

}

//检测程序下载OK后进入BOOT程序更新
//size-固件大小，sumer-固件累加校验和
bool app_enter_update(uint32_t size,uint16_t sumer)
{
	if(check_date_sumer_flash(FW_DOWNLOAD_ADDR_EXT,size,sumer)==0)	//检查写入flash数据是否正确
	{
	    LOG("sumer check error!!!\r\n");
	    return false;
	}

	memset(&system_parm.update_flag[0],FLAG_ENTER_UPDATE,4);
	system_parm.file_len=size;

	BSP_W25Qx_Write((uint8_t*)&system_parm,SYS_CONFIG_ADDR ,sizeof(system_parm));
	LOG("enter update!!!\r\n");
	delay_ms(500);
	NVIC_SystemReset(); 		//复位运行boot程序

}
/*
* 判断URL是IP地址还是域名
* return：地址为IP返回true 其他返回false
*/
bool server_is_ip(char *server)
{
	char ip[4][4] = {0};
	int  ip_i = 0;
	char *ptr = server;
	int  ptr_i = 0;
	int  ptr_len = strlen(server);
    
	if (NULL == server || 15 < ptr_len)
	    return false;

    //检查IP范围
    for (int i=0; i<4; i++)
    {
        ip_i = 0;
        for(int j=0; j<4; j++)
        {
            //1.字符为0-9的数字
            if (3 != j && isdigit(ptr[ptr_i]))
            {
 				ip[i][ip_i++] = ptr[ptr_i++];
            }
            //2.字符为'.'分隔符
            else if (3 != i && 0 != j && '.' == ptr[ptr_i])
            {
                ip[i][ip_i++] = '\0';
                ptr_i++;
                break;
            }
            else
            {
                return false;
            }
            //3.结束
            if (ptr_len <= ptr_i)
            {
                if (3 == i)
                  break;
                else
                  return false;
            }
         }
    }
    //检查IP格式
    for (int z=0; z<4; z++)
    {
        if (atoi(ip[z]) > 255)
            return false;
    }
    return true;
}


/*
*解析OTA升级命令
*data_buff:需要解析的数据
*return：1-解析成功，0-数据错误
*/
bool ota_cmd_analysis(char* data_buff)
{
	char *host_str;
	
	memset(&ota_info,0,sizeof(OTA_INFO));
	
	if(sscanf(data_buff,"OTA:URL=%[^,],MD5=%[^,],SIZE=%d",
	ota_info.url,ota_info.md5,&ota_info.file_size)==3)//解析参数
	{
		if(ota_info.file_size==0||strlen(ota_info.md5)!=32)			//数据不正常
		{
			LOG("error：size/md5");
			return false;
		}
		
		if(sscanf(ota_info.url,"http://%[^/]%s", ota_info.host,ota_info.path)==2)//解析url
		{		
			host_str=pvPortMalloc(100);
			memset(host_str,0,100);
			if(sscanf(ota_info.host,"%[^:]:%d",host_str,&ota_info.port)!=2)
			{
				LOG("the host does not contain a port\r\n");
				ota_info.port=HTTP_DEFAULT_PORT;//http 默认端口为80
			}
			else
			{
				strcpy(ota_info.host,host_str);
				
			}
			
			if(dtu_rtos_classes==USE_CLASSES_ETH_FLAG)//eth环境下需解析域名为IP
			{
				if(server_is_ip(ota_info.host)==true)//检测是否为IP地址
				{
					
					ota_info.ip.addr=ipaddr_addr(ota_info.host);
					LOG("IP ADDR=%s\r\n",ip4addr_ntoa((ip4_addr_t *)&ota_info.ip.addr));
				}
				else 
				{
					//转换为IP地址
					err_t res=get_host_by_name(ota_info.host,&ota_info.ip);
					if(res!=ERR_OK)
					{
						LOG("get ip error");
						vPortFree(host_str);
						return false;
					}
					
				}
			}
			vPortFree(host_str);
			//解析成功，返回true调用http升级功能
			return  true;
		}
		else
		{
			LOG("IP error");
			return false;
		}
	}
	else
	{
		//LOG("unknow cmd");
		return false;

	}
}

//将计算得出的MD5值转换成字符串后与预期MD5对比是否一致，是则返回true
bool check_md5Result(char* expect_md5,uint8_t* md5_t)
{
	uint8_t i=0;
	char *md5_result;
	char md5_t1[4] = {0, 0, 0, 0};
	md5_result=pvPortMalloc(36);//申请内存
	
	memset(md5_result,0,36);
	for(i = 0; i < 16; i++)
	{
		if(md5_t[i] <= 0x0f)
			sprintf(md5_t1, "0%X", md5_t[i]);
		else
			sprintf(md5_t1, "%X", md5_t[i]);
		
		strcat(md5_result, md5_t1);
	}

	LOG( "result MD5: %s\r\n", md5_result);
	LOG( "expect MD5: %s\r\n", expect_md5);


	if(strcmp(md5_result, expect_md5) == 0)																			//MD5校验比对
	{
		vPortFree(md5_result);//释放内存
		LOG( "Tips: MD5 Successful Matches\r\n");
		return true;
	}
	else
	{
		vPortFree(md5_result);//释放内存
		return false;
	}
}

/*
* 获取http返回的数据长度
*/
uint16_t get_http_data_len(void)
{
	switch (dtu_rtos_classes)
	{
	case USE_CLASSES_OFF:
		return 0;
	case USE_CLASSES_4G_FLAG:
		return 0;
	case USE_CLASSES_ETH_FLAG:
		return eth_http_parm.recvbuf_pos;
	case USE_CLASSES_WIFI_FLAG:
	
		return 0;
	default:
	return 0;
	}
}

/*
* 获取http接收的数据首地址
*/
uint8_t *get_http_data_addr(void)
{
	switch (dtu_rtos_classes)
	{
	case USE_CLASSES_OFF:
		return 0;
	case USE_CLASSES_4G_FLAG:
		return 0;
	case USE_CLASSES_ETH_FLAG:
		return eth_http_parm.recv_buf;
	case USE_CLASSES_WIFI_FLAG:
	
		return 0;
	default:
	return 0;
	}
}
/*
*清除http接收的数据及长度
*/
void clear_http_recv_data(void)
{
	switch (dtu_rtos_classes)
	{
	case USE_CLASSES_OFF:
	break;
	case USE_CLASSES_4G_FLAG:
	break;
	case USE_CLASSES_ETH_FLAG:
		clear_eth_ota_recv_data();
	break;
	case USE_CLASSES_WIFI_FLAG:
	break;
	default:
	break;
	}
}
/*
*向http服务器发送数据
*/
bool http_send_data(char *buff)
{
	switch (dtu_rtos_classes)
	{
	case USE_CLASSES_OFF:
	return false;
	case USE_CLASSES_4G_FLAG:
	return false;
	case USE_CLASSES_ETH_FLAG:
		
	return eth_http_send(buff);

	case USE_CLASSES_WIFI_FLAG:
	return false;
	default:
	return false;
	}

}
/*************************************************************
*	函数名称： HTTP_Download_Range
*	函数功能： 分片下载固件
*	入口参数： url：固件的url地址
*				md5：平台返回的MD5
*				size：平台返回的固件大小(字节)
*				bytes_range：分片大小(字节)
*	返回参数： 1-成功	其他-失败
*	说明：url:http://ota.heclouds.com/ota/south/download/ota_QKGRhntBPCtMH1MEA7ft
http://ota.heclouds.com/ota/south/download/ota_T0wwKPd1IkkEsRaw4U9S
*************************************************************/
char HTTP_Download_Range(OTA_INFO ota_info1,  uint16_t bytes_range)
{											//MD5相关变量
	MD5_CTX md5_ctx;											//MD5相关变量
	unsigned char  md5_t[16];

	uint16_t sumer=0;

	char *send_buf;
	char *data_ptr = NULL;
	unsigned int time_out = 200;
	unsigned int bytes = 0; //已写入数据大小
	unsigned char err_cnt = 0;
	
	uint16_t recv_bin_len; //接收的固件包长度
    uint16_t pack_len=bytes_range;		//每包下载数据长度
	update_info.flash_write_addr=FW_DOWNLOAD_ADDR_EXT; 

	//----------------------------------------------------MD5初始化、擦除闪存-----------------------------------------------------------
	MD5_Init(&md5_ctx);
		
	send_buf=pvPortMalloc(SEND_BUFF_MAX_SIZE);
	clear_http_recv_data(); //先清除数据
	while(bytes < ota_info1.file_size)
	{
//----------------------------------------------------发送报文下载一包数据----------------------------------------------------------
		memset(send_buf, 0, SEND_BUFF_MAX_SIZE);
		
		if(ota_info1.file_size < bytes_range)//文件小于单包大小
		{
			pack_len=ota_info1.file_size;
		}
		if(bytes > ota_info1.file_size- bytes_range)	//最后一包数据
        {
        	pack_len=ota_info1.file_size-bytes;		//最后一包数据长度
        }
    	snprintf(send_buf, SEND_BUFF_MAX_SIZE, "GET %s HTTP/1.1\r\n"
					"Range:bytes=%d-%d\r\nConnection: Keep-alive\r\n\r\n",
					ota_info.path, bytes, bytes + pack_len - 1);
		
    	if(err_cnt >= 5)
		{
			err_cnt = 0;
			LOG("http error!");
			return 0;
		}
		
		LOG(send_buf);
		data_ptr=NULL;
		//err = netconn_write(tcp_clientconn ,send_buf,strlen((char*)send_buf),NETCONN_COPY); //发送tcp_server_sentbuf中的数据
		if(http_send_data(send_buf) == false)
		{
			err_cnt++;
			LOG("http send fail!\r\n");
		}
		else
		{
//----------------------------等待数据---------------------------------------------------------------------
			time_out = 100;
			while(--time_out)
			{
				if(get_http_data_len()>pack_len)//判断是否接收到足够数据
					break;
				vTaskDelay(10);
			}
//-----------------------------分析HTTP报文头，跳过HTTP报文头、找到固件数据---------------------------------------------
			if(time_out)//未超时，说明接收到数据
			{
				//LOG("http data:%s",get_http_data_addr()); //打印http返回的数据
				data_ptr = strstr((char*)get_http_data_addr(), "HTTP/1.1 20");
				//判断是否接收到正确报文
				if(data_ptr==NULL)
				{
				 	LOG("http head error\r\n ");
				 	err_cnt++;
					continue;//数据错误，重新发送接收
				}
			
				data_ptr = strstr(data_ptr, "\r\n\r\n");//获取第一个\r的地址
				data_ptr+=4;//跳过HTTP报文头、找到固件数据首地址
				recv_bin_len=(uint16_t)((char*)(get_http_data_addr()+get_http_data_len())-data_ptr);//通过地址长度获得包数据长度。
	            
				time_out = 50;
				while(--time_out)
	            {
	                if(recv_bin_len==pack_len)		//判断数据是否接收完整
	                    break;
	                
	                delay_ms(10);//等待一段时间看是否有后续数据
	               	recv_bin_len=(uint16_t)((char*)(get_http_data_addr()+get_http_data_len())-data_ptr);//通过地址长度获得包数据长度。
	          
	            }
	            if(time_out==0)
				{
	                LOG(" error:rec bin data len=%d\r\n",recv_bin_len);
	                data_ptr = NULL;
	                err_cnt++;
	            }
	            LOG("Rec data len=%d \r\n",get_http_data_len());
	            LOG("bin pack len=%d \r\n",recv_bin_len);
	            	
			}
			else
			{
				LOG("RECV TIMEOUT!\r\n");
				err_cnt++;
			}
			if(data_ptr != NULL)
			{//数据检测正常，可以写入flash
				err_cnt = 0;
		
				MD5_Update(&md5_ctx, (unsigned char *)data_ptr, pack_len);
				sumer+=get_pack_sumer((uint8_t*)data_ptr, pack_len);
				BSP_W25Qx_Write((uint8_t*)data_ptr,update_info.flash_write_addr,pack_len);
			
				bytes += bytes_range;
				update_info.flash_write_addr += bytes_range;
			
				ota_display(bytes,ota_info1.file_size);
			}
			//处理完成，清空数据
			clear_http_recv_data(); //清除前一帧数据
		}
	}
	vPortFree(send_buf);//释放内存
		
	//--------------------------MD校验比对------------------------------------------------------------------
	MD5_Final(&md5_ctx, md5_t);

	if(check_md5Result(ota_info1.md5,md5_t)){
		return app_enter_update(ota_info1.file_size,sumer);
	}
	else
		return 0;
}

/*
* HTTP下载固件任务
*/
void task_http_download(void *arg)
{
 	char res=0;
 	osThreadState_t os_state;
 	while(1)
	{
		LOG("http download task start...\r\n");
		res=HTTP_Download_Range(ota_info,W25Q128FV_SUBSECTOR_SIZE);
		if(res!=1)
		{
			LOG("OTA Fail!\r\n");
			os_state = osThreadGetState(task_http_download_handle);
			LOG("http_download os_state=%d\r\n",os_state);
			delete_eth_ota_thread();
			
			osStatus_t task_state =osThreadSuspend(task_http_download_handle);//挂起自身任务
		}
		else
		{
			LOG("OTA Success!\r\n");
			osStatus_t task_state =osThreadSuspend(task_http_download_handle);
		}

		delay_ms(100);
	}

}


osThreadId_t task_http_download_create(void)
{
	return osThreadNew(task_http_download, NULL, &http_download_task_attributes);

}

#if 0

/*************************************************************
*	函数名称： WiFi_Download_FW
*	函数功能： wifi方式分片下载固件
*	入口参数：ota_info1: ota相关参数
*				bytes_range：分片大小(字节)
*	返回参数： 1-成功	其他-失败
*	说明：url:http://ota.heclouds.com/ota/south/download/ota_QKGRhntBPCtMH1MEA7ft
http://ota.heclouds.com/ota/south/download/ota_T0wwKPd1IkkEsRaw4U9S
*************************************************************/
char WiFi_Download_FW(OTA_INFO ota_info1, uint16_t bytes_range)
{	
	//MD5相关变量
	MD5_CTX md5_ctx;										
	unsigned char md5_t[16];

	uint16_t sumer=0;
	char *send_buf;
	char *data_ptr = NULL;
	unsigned int time_out = 200;
	unsigned int bytes = 0; //已写入数据大小
	unsigned char err_cnt = 0;
	
	uint16_t recv_bin_len; //接收的固件包长度
    uint16_t pack_len=bytes_range;		//每包下载数据长度
    
	update_info.flash_write_addr=FW_DOWNLOAD_ADDR_EXT; 
	send_buf=pvPortMalloc(SEND_BUFF_MAX_SIZE);//申请内存
	
	if(ota_info1.file_size< bytes_range)//文件小于单包大小
	{
		pack_len=ota_info1.file_size;
	}

	//----------------------MD5初始化----------------------------------------------
	MD5_Init(&md5_ctx);

	while(bytes < ota_info1.file_size)
	{
	//----------------------------发送报文下载一包数据-----------------------------
		memset(send_buf, 0, SEND_BUFF_MAX_SIZE);
		
		if(bytes+ bytes_range > ota_info1.file_size)	//最后一包数据
        {
        	pack_len=ota_info1.file_size-bytes;		//最后一包数据长度
        }
    	snprintf(send_buf, SEND_BUFF_MAX_SIZE, "GET %s HTTP/1.1\r\n"
					"Range:bytes=%d-%d\r\nConnection: Keep-alive\r\n\r\n",
					ota_info1.path, bytes, bytes + pack_len - 1);
		
    	if(err_cnt >= 5)
		{
			err_cnt = 0;
			vPortFree(send_buf);//释放内存
		
			LOG("http error!");
			return 0;
		}
		
		//tcp_recv_flag=false;//等待TCP数据接收
		LOG(send_buf);
		data_ptr=NULL;
		USART_WiFi_Send((uint8_t*)send_buf);//发送get请求
		{
//----------------------------等待数据---------------------------------------------------------------------
			time_out = 200;
			while(--time_out)
			{
				if(usart_wifi_rx_count>pack_len)//判断是否接收到足够数据
					break;
				vTaskDelay(10);
			}
//-----------------------------分析HTTP报文头，跳过HTTP报文头、找到固件数据---------------------------------------------
			if(time_out)//未超时，说明接收到数据
			{
				//USART1_Send(tcp_client_recvbuf);
				data_ptr = strstr((char*)usart_wifi_rx_buff, "HTTP/1.1 20");
				//判断是否接收到正确报文
				if(data_ptr==NULL)
				{
				 	LOG("http head error ");
				 	err_cnt++;
					continue;//数据错误，重新发送接收
				}
			
				data_ptr = strstr(data_ptr, "\r\n\r\n");//获取第一个\r的地址
				data_ptr+=4;//跳过HTTP报文头、找到固件数据首地址
				recv_bin_len=(uint16_t)((char*)usart_wifi_rx_buff+usart_wifi_rx_count-data_ptr);//通过地址长度获得包数据长度。
	            
				time_out =50;
				while(--time_out)
	            {
	                if(recv_bin_len==pack_len)		//判断数据是否接收完整
	                    break;
	                
	                delay_ms(10);//等待一段时间看是否有后续数据
	               	recv_bin_len=(uint16_t)((char*)usart_wifi_rx_buff+usart_wifi_rx_count-data_ptr);//通过地址长度获得包数据长度。
	          
	            }
	            if(time_out==0)
				{
	                LOG(" error:rec bin data len=%d",recv_bin_len);
	                data_ptr = NULL;
	                err_cnt++;
	            }
	            LOG("Rec data len=%d ",usart_wifi_rx_count);
	            LOG("bin pack len=%d ",recv_bin_len);
	            	
			}
			else
			{
				LOG("RECV TIMEOUT!");
				err_cnt++;
			}
			if(data_ptr != NULL)
			{//数据检测正常，可以写入flash
				err_cnt = 0;
				MD5_Update(&md5_ctx, (unsigned char *)data_ptr, pack_len);
				sumer+=get_pack_sumer((uint8_t*)data_ptr,pack_len);
				W25QXX_Write(update_info.flash_write_addr,(uint8_t*)data_ptr, pack_len);
			
				bytes += bytes_range;
				update_info.flash_write_addr += bytes_range;
			
				OTA_Display(bytes, ota_info1.file_size);
			}
			//处理完成，清空数据
			usart_wifi_rx_count=0;
			memset(usart_wifi_rx_buff,0,USART_WIFI_BUFFER_SIZE);
		}
	}
	vPortFree(send_buf);//释放内存
		
	//--------------------------MD校验比对------------------------------------------------------------------
	MD5_Final(&md5_ctx, md5_t);

	if(check_md5Result(ota_info1.md5,md5_t)){
		app_enter_update(ota_info1.file_size,sumer);
		return 1;
	}
	else
		return 0;
}
#endif


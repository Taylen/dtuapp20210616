 /*
 * File      : webclient_post.c
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * zengxiaohui43@163.com
 * Change Logs:
 * Date           Author       Notes
 * 2021-05-30     zeng     first version
 */

#include <string.h>
#include <stdio.h>
#include <webclient.h>
#include <stdlib.h>
#include "cmsis_os.h"
#define POST_RESP_BUFSZ                1024
#define POST_HEADER_BUFSZ              1024

#define POST_LOCAL_URI                 "http://192.168.0.33:8983/yeszn-web/dtuInitialization"

web_client_t my_client;

/* send HTTP POST request by simplify request interface, it used to received shorter data */
#if 0
static int webclient_post_smpl(const char *uri, const char *post_data)
{
	return 0;
}

int webclient_post_test(void)
{

	return 0;
}

int do_webclient_post(int argc, char **argv)
{
    char *uri = NULL;

    if (argc == 1)
    {

    }
    else if (argc == 2)
    {

    }
    else if(argc == 3 && strcmp(argv[1], "-s") == 0)
    {

    }
    else
    {

    }
    

    return 0;
}

#endif


﻿/*
 * File      : version.c
 * Change Logs:
 * Date           Author       Notes
 * 2021-05-30     zeng     first version
 */

#include <stdio.h>
#include "bsp_version.h"
#include "stm32f4xx_hal.h"
#include "usart.h"



const unsigned char * asc_banner;
const unsigned char version_string[] = MK_STR(MONITOR_VERSION);

// 软件版本
const char ver_version = VERSION;
const char ver_sublevel = SUBLEVEL;
const char ver_patchlevel = PATCHLEVEL;




/*
 * [COMMAND]
 *
 * 返回值：
 *		0：OK；
 *		-1：error.
 */
int do_version(int argc, char *argv[])
{
	printf("\nVer: %s,  %s, %s  \r\n", version_string, __DATE__, __TIME__);
	printf("CPU:STM32F4(running at %d MHz)\r\n", HAL_RCC_GetSysClockFreq()/1000000);
	printf("Current PC: 0x%08X, Current SP: 0x%08X\n\n", __current_pc(), __current_sp());	
	return(0);
}






















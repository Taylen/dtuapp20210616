
#include "app_wifi.h"	
#include "delay.h"
#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "app_ota.h"


#define   WIFI_TASK_PRIO      5
#define   WIFI_STK_SIZE      256
TaskHandle_t WIFITask_Handler;
WIFI_DEVICE_INFO wifi_device_info;


#define   WIFI_OTA_TASK_PRIO      6
#define   WIFI_OTA_STK_SIZE      400
TaskHandle_t WIFI_OTATask_Handler;


WIFI_DEVICE_INFO wifi_device_info;

void Task_wifi_ota_creat(void);
//WIFI IO初始化
void GPIO_Wifi_Init(void)
{   
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);//使能GPIOF时钟

	//初始化WIFI模块对应引脚
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7|GPIO_Pin_8|GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100MHz
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;//下拉
	GPIO_Init(GPIOE, &GPIO_InitStructure);//初始化GPIO

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13|GPIO_Pin_14;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;//普通输入模式
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100MHz
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;//上拉
	GPIO_Init(GPIOE, &GPIO_InitStructure);//初始化GPIO

	GPIO_ResetBits(PIN_WIFI_POWER);//低有效，供电。
  	GPIO_ResetBits(PIN_WIFI_NRELOAD);
}
//wifi恢复出厂设置
void Wifi_Reload(void)
{
	delay_ms(3000);
	GPIO_SetBits(PIN_WIFI_NRELOAD);//拉高5秒复位
	delay_ms(3000);
	GPIO_ResetBits(PIN_WIFI_NRELOAD);
}
//初始化wifi状态，ap mode
void Wifi_init_cmd(void)
{
	USART_WiFi_Send(CMD_WIFI_APMODE);
	delay_ms(500);
	USART_WiFi_Send(CMD_WIFI_REBOOT);

}

void Wifi_Reboot_cmd(void)
{

	USART_WiFi_Send(CMD_WIFI_REBOOT);

}
//检测wifi模块是否启动
bool check_wifi_ready(void)
{
	if(WIFI_READY==1)
	{
		return true;
	}
	else
	{
		return false;
	}

}
//检测WiFi是否连接
bool check_wifi_connect(void)
{
	if(WIFI_CONNECT==1)
	{
		return true;
	}
	else
	{
		return false;
	}

}



//==========================================================
//	函数名称：	WIFI_DEVICE_SendCmd
//
//	函数功能：	向网络设备发送一条命令，并等待正确的响应
//
//	入口参数：	cmd：需要发送的命令
//				res：需要检索的响应
//				timeOut:超时检测时间
//	返回参数：	返回连接结果
//
//	说明：		1-成功		0-失败
//==========================================================
_Bool WIFI_DEVICE_SendCmd(char *cmd, char *res, int timeOut)
{
	 u32 i;
    u8 times=20;
    u8 cmd_error_count=0;
    timeOut = timeOut/times;
    
    while(1)
    {
        USART_Wifi_Clear();
        USART_WiFi_Send((u8*) cmd);   

        for(i=0;i<timeOut;i++)
        {
            if((NULL != strstr((char*)usart_wifi_rx_buff, res)))	//判断是否有预期的结果
            {
            	USART1_Send(usart_wifi_rx_buff);
                USART_Wifi_Clear(); // printf("%s: %s\r\n",cmd,result);
              
                return 1;
            }
            else if((NULL != strstr((char*)usart_wifi_rx_buff, "ERROR")))	
            {
               USART_Wifi_Clear();// printf("ERROR:%s",cmd);
                         
               delay_ms(200);        
               break;
            } 
            else
            {  
                delay_ms(times);
            }
           
        }
        //无效数据情况
        if(++cmd_error_count>=5)  //不成功则重启模块
        {         
            DebugPrintf("AT指令无有效数据,重置\r\n");
			//Wifi_Reload();

            return 0;
        }
    }

}

bool check_tcp_connect(void)
{
	if(WIFI_DEVICE_SendCmd(CMD_WIFI_TCPLK,"+ok=on",3000))
	{
		return true;
		
	}
	else
		return false;
}
//==========================================================
//	函数名称：	NET_DEVICE_Connect
//
//	函数功能：	重连平台
//
//	入口参数：	type：TCP 或 UDP
//				ip：IP地址缓存指针
//				port：端口缓存指针
//
//	返回参数：	返回连接结果
//
//	说明：		0-成功		1-失败
//==========================================================
_Bool WiFi_Connect_TCP( char *ip, char *port)
{
	char cmd[64];
	char i;

	sprintf(cmd,"AT+NETP=TCP,Client,%s,%s\r\n",port,ip);
	for(i=0;i<5;i++)
	{
		if(WIFI_DEVICE_SendCmd(cmd,"+ok",1000))
		{
			 if( WIFI_DEVICE_SendCmd(CMD_WIFI_REBOOT,"+ok", 3000))
			 {//重启命令生效
				delay_ms(1000);
			 	return true;
			 }
		}
	}
	return false;

}

//WiFi模块连网初始化
u8 wifi_net_initial(void)
{  
	char res;
	u8 fail_count=0;
	
	while(!wifi_device_info.init_type)
	{
	   if(fail_count>=10)  			//连续错误退出初始化
	   {  
			DebugPrintf("AT CMD ERROR!!!"); 
			fail_count=0;
			wifi_device_info.init_step=0;
			return false;
	   }
	   switch(wifi_device_info.init_step)
	   {
		 	case STEP_INIT1:		  
			   res= WIFI_DEVICE_SendCmd(CMD_WIFI_INIT1,"a", 1000); 			
			   if(res==0)								  //重置
			   {
				   	fail_count++; 								//错误累计
			   }
			   else
			   {
			   		fail_count=0;
					wifi_device_info.init_step++;
			   }
			   break;
			  
		   case STEP_INIT2:
				res= WIFI_DEVICE_SendCmd(CMD_WIFI_INIT2,"+ok", 1000); 
				if(res==0)								  //重置
				{
					fail_count++; 								//错误累计
				}
				else
				{
					fail_count=0;
					wifi_device_info.init_step++;
				}
			   break;
			   
		   case STEP_STAMODE:
			   res= WIFI_DEVICE_SendCmd(CMD_WIFI_STAMODE,"+ok", 3000);		
			    if(res==0)								  //重置
			   {
				   	fail_count++; 								//错误累计
			   }
			   else
			   {
			   		fail_count=0;
					wifi_device_info.init_step++;
			   }
			   break;
			case STEP_WSSSID:
			   res= WIFI_DEVICE_SendCmd(CMD_WIFI_WSSSID,"+ok", 3000);		
			    if(res==0)								  //重置
			   {
				   	fail_count++; 								//错误累计
			   }
			   else
			   {
			   		fail_count=0;
					wifi_device_info.init_step++;
			   }
			   break;
			case STEP_WSKEY:
			   res= WIFI_DEVICE_SendCmd(CMD_WIFI_WSKEY,"+ok", 3000);		
			    if(res==0)								  //重置
			   {
				   	fail_count++; 								//错误累计
			   }
			   else
			   {
			   		fail_count=0;
					wifi_device_info.init_step++;
			   }
			   break;
			case STEP_TCP:
			   res= WIFI_DEVICE_SendCmd(CMD_WIFI_TCP,"+ok", 3000);		
			    if(res==0)								  //重置
			   {
				   	fail_count++; 								//错误累计
			   }
			   else
			   {
			   		fail_count=0;
					wifi_device_info.init_step++;
			   }
			   break;
			case STEP_REBOOT:
			   res= WIFI_DEVICE_SendCmd(CMD_WIFI_REBOOT,"+ok", 3000);		
			   if(res==0)								  //重置
			   {
				   	fail_count++; 								//错误累计
			   }
			   else
			   {
			   		fail_count=0;
					wifi_device_info.init_step=0;
					wifi_device_info.init_type=1;
			   }
			   break;
	   }
	}
	return true;
}


//wifi任务
void WIFI_NET_Task(void *pvParameters)
{
	GPIO_Wifi_Init();
	while(1)
	{
		if(check_wifi_ready())
		{
			//DebugPrintf("WIFI RUN!");
			if(!check_wifi_connect())
			{
				wifi_net_initial();
				delay_ms(15000);
			}
		}
		delay_ms(1000);
		
	}

}




void Task_wifi_creat(void)
{

	taskENTER_CRITICAL();              
	 xTaskCreate((TaskFunction_t)WIFI_NET_Task,
					(const char*  )"wifi_task",
					(uint16_t     )WIFI_STK_SIZE,
					(void*        )NULL,
					(UBaseType_t  )WIFI_TASK_PRIO,
					(TaskHandle_t*)&WIFITask_Handler);
	taskEXIT_CRITICAL();
}

//判断处理wifi下发的cmd
void handle_wifi_cmd(char *data)
{ //ota cmd--- AT+UPDATE:URL=http://192.168.0.163/LWIP.bin,MD5=5234bbdb36ef150ecb1d6589412b6951,SIZE=1212121.
	
    if(strstr((char*)data,"UPDATE")) //版本查询
	{
		if(OTA_CMD_Analysis(data)){
			Task_wifi_ota_creat();
		}else{
			DebugPrintf("Update命令解析失败");
		}
	}
}

//处理USART1接收的命令
void wifi_recv_handle(char *buff)
{
	if(buff[0]=='A'&&buff[1]=='T'&&buff[2]=='+')//命令头
	{
		handle_wifi_cmd(buff);
	}
}


void wifi_ota_task()
{
	u8 timeout=60;

	WIFI_DEVICE_SendCmd(CMD_WIFI_INIT1,"a", 1000); 	
	WIFI_DEVICE_SendCmd(CMD_WIFI_INIT2,"+ok", 1000);
	delay_ms(10);
	WIFI_DEVICE_SendCmd(CMD_WIFI_TCPDIS,"+ok", 1000);
	
	if(WiFi_Connect_TCP(ota_info.host,"80")==false)
	{
		DebugPrintf("连接不上TCP");
		vTaskDelete(WIFI_OTATask_Handler);
		delay_ms(10);
	}
	timeout=60;//60S检测时间
	while(timeout--)
	{//重启后等待wifi连接
		if(check_wifi_connect())
		{
			DebugPrintf("wifi已连接");
			break;
		}
		delay_ms(1000);
		if(timeout==0)
		{
			DebugPrintf("连接不上wifi");
			vTaskDelete(WIFI_OTATask_Handler);
			delay_ms(10);
		}
	}
	//wifi连接成功，
	WIFI_DEVICE_SendCmd(CMD_WIFI_INIT1,"a", 1000); 	
	WIFI_DEVICE_SendCmd(CMD_WIFI_INIT2,"+ok", 1000);
	timeout=60;//60S检测时间
	while(timeout--)
	{//检测tcp是否连接
		if(check_tcp_connect())
		{
			break;
		}
		delay_ms(1000);
		if(timeout==0)
		{
			DebugPrintf("连接不上ip");
			vTaskDelete(WIFI_OTATask_Handler);
			delay_ms(10);
		}
	}
	WIFI_DEVICE_SendCmd(CMD_WIFI_ENTM,"+ok", 1000);//进入透传模式
	
	delay_ms(1000);

	//TCP连接，正式进入OTA升级。
	if(WiFi_Download_FW(ota_info,W25Q128_SECTOR_SIZE)==false)
	{
		DebugPrintf("HTTP error!!!");
		vTaskDelete(WIFI_OTATask_Handler);
		delay_ms(10);
	}

}

void Task_wifi_ota_creat(void)
{

	taskENTER_CRITICAL();              
	 xTaskCreate((TaskFunction_t)wifi_ota_task,
					(const char*  )"WIFI_ota_task",
					(uint16_t     )WIFI_OTA_STK_SIZE,
					(void*        )NULL,
					(UBaseType_t  )WIFI_OTA_TASK_PRIO,
					(TaskHandle_t*)&WIFI_OTATask_Handler);
	taskEXIT_CRITICAL();
}





/* Includes ------------------------------------------------------------------*/
#include "bsp_tcp_client.h"
#include "dns.h"
#include "app_ota.h"
#include "delay.h"
//忽略mdk waring #870
#pragma diag_suppress 870 
//task
extern osSemaphoreId_t tcp_recv_semhandle;

const osThreadAttr_t tcp_client_task_attributes = {
  .name = "tcp_client",
  .priority = (osPriority_t) osPriorityBelowNormal,
  .stack_size = 512
};

osThreadId_t task_eth_ota_handle;
const osThreadAttr_t eth_ota_task_attributes = {
  .name = "eth_ota",
  .priority = (osPriority_t) osPriorityBelowNormal3,
  .stack_size = 3000
};


TcpParameter tcp_parm;	
HTTP_PARM eth_http_parm;	  //ota http

uint8_t server_ip[4]={192,168,0,165};

static struct netconn *tcp_clientconn;//TCP CLIENT网络连接结构体
static struct netconn *tcp_http_conn; //http server网络连接结构体

void eth_ota_thread(void *arg);

/*
*设置要链接的TCP IP & port
*/
void tcp_client_set_remoteip(uint8_t ip[4],uint16_t port)
{
	tcp_parm.remoteip[0]=ip[0];
	tcp_parm.remoteip[1]=ip[1];
	tcp_parm.remoteip[2]=ip[2]; 
	tcp_parm.remoteip[3]=ip[3];

	tcp_parm.server_port=port;
	LOG("Set Remote IP:%d.%d.%d.%d,Port:%d\r\n",tcp_parm.remoteip[0],tcp_parm.remoteip[1],
	tcp_parm.remoteip[2],tcp_parm.remoteip[3],port);//远端IP

}

//tcp客户端任务函数
void tcp_client_thread(void *arg)
{
	struct pbuf *q;
	err_t err,recv_err;

	static ip_addr_t server_ipaddr,loca_ipaddr;
	static u16_t 		 loca_port;
		
	LWIP_UNUSED_ARG(arg);
	tcp_client_set_remoteip(server_ip,DEFAULT_PORT);
	IP4_ADDR(&server_ipaddr, tcp_parm.remoteip[0],tcp_parm.remoteip[1], 
	tcp_parm.remoteip[2],tcp_parm.remoteip[3]);
	
	while (1) 
	{
		LOG("TCP CONNECTING\r\n");
		tcp_clientconn=netconn_new(NETCONN_TCP);  //创建一个TCP链接
        err = netconn_connect(tcp_clientconn,&server_ipaddr,tcp_parm.server_port);//连接服务器
		if(err != ERR_OK)
		{
			netconn_delete(tcp_clientconn); //返回值不等于ERR_OK,删除tcp_clientconn连接
		}
		else if (err == ERR_OK)    //处理新连接的数据
		{ 
			struct netbuf *recvbuf;
			tcp_clientconn->recv_timeout = 10;
			netconn_getaddr(tcp_clientconn,&loca_ipaddr,&loca_port,1); //获取本地IP主机IP地址和端口号
			LOG("connect server %d.%d.%d.%d successful,local port:%d\r\n",
			tcp_parm.remoteip[0],tcp_parm.remoteip[1], tcp_parm.remoteip[2],
			tcp_parm.remoteip[3],loca_port);
			while(1)
			{
			
				if(tcp_parm.send_flag == true) //有数据要发送
				{
					err = netconn_write(tcp_clientconn ,
					tcp_parm.send_buf,strlen((char*)tcp_parm.send_buf),NETCONN_COPY); //发送tcp数据
					if(err != ERR_OK)
					{
						LOG("tcp send data fail!");
					}
					tcp_parm.send_flag =false;
				}
					
				if((recv_err = netconn_recv(tcp_clientconn,&recvbuf)) == ERR_OK)  //接收到数据
				{	
					taskENTER_CRITICAL();
					if(tcp_parm.recv_flag==false)//新数据进来，tcp_recv_flag为true时旧数据未处理，数据会叠加。
					{
						tcp_parm.recvbuf_pos=0;
						memset(tcp_parm.recv_buf,0,TCP_CLIENT_RX_BUFSIZE);  //数据接收缓冲区清零
					}
					for(q=recvbuf->p;q!=NULL;q=q->next)  //遍历完整个pbuf链表
					{
						//判断要拷贝到TCP_CLIENT_RX_BUFSIZE中的数据是否大于TCP_CLIENT_RX_BUFSIZE的剩余空间，如果大于
						//的话就只拷贝TCP_CLIENT_RX_BUFSIZE中剩余长度的数据，否则的话就拷贝所有的数据
						if(q->len > (TCP_CLIENT_RX_BUFSIZE-tcp_parm.recvbuf_pos)) 
							memcpy(tcp_parm.recv_buf+tcp_parm.recvbuf_pos,q->payload,(TCP_CLIENT_RX_BUFSIZE-tcp_parm.recvbuf_pos));//拷贝数据
						else 
							memcpy(tcp_parm.recv_buf+tcp_parm.recvbuf_pos,q->payload,q->len);
						tcp_parm.recvbuf_pos += q->len;  	
						if(tcp_parm.recvbuf_pos > TCP_CLIENT_RX_BUFSIZE) break; //超出TCP客户端接收数组,跳出	
					}
					
					tcp_parm.recv_flag=true;
					taskEXIT_CRITICAL();
					osSemaphoreRelease (tcp_recv_semhandle);
					netbuf_delete(recvbuf);
				}
				else if(recv_err == ERR_CLSD)  //关闭连接
				{
					netconn_close(tcp_clientconn);
					netconn_delete(tcp_clientconn);
					LOG("Server %d.%d.%d.%d disconnect\r\n",tcp_parm.remoteip[0],tcp_parm.remoteip[1],
					tcp_parm.remoteip[2],tcp_parm.remoteip[3]);
					break;
				}
			}
		}
	}
}


osThreadId_t tcp_client_task_create(void)
{
	return osThreadNew(tcp_client_thread, NULL, &tcp_client_task_attributes);

}


/*
*分析处理TCP接收的命令
*buff:需要处理的数据
*/
void tcp_recv_data_handle(uint8_t *buff)
{
	if(strstr((char*)buff,"OTA:"))//是否含有OTA数据
	{
		LOG("ota data received\r\n");
		if(ota_cmd_analysis((char*)buff))
		{
			LOG("start ota task!\r\n");
			LOG("host=%s \r\npath=%s \r\nport=%d \r\nip=%s \r\nmd5=%s \r\nfile size=%d \r\n",
			ota_info.host,ota_info.path,ota_info.port,ip_ntoa(&ota_info.ip),
			ota_info.md5,ota_info.file_size);

			osThreadState_t os_state = osThreadGetState(task_eth_ota_handle);
						LOG("eth_ota_task os_state=%d\r\n",os_state);

			task_eth_ota_handle=osThreadNew(eth_ota_thread, NULL, &eth_ota_task_attributes);
		}
		tcp_parm.recv_flag=false;
	}
}

/*
*域名解析
*hostname:需要解析的域名
*addr:解析出的ip地址
*/
err_t get_host_by_name(const char *hostname,ip_addr_t *addr)
{
	err_t result;
	
	result=netconn_gethostbyname((char *)hostname, addr);
	LOG("get_host_by_name:%s,result=%d\r\n",hostname,result);
    if(result== ERR_OK)
    {
    	LOG("get ip addr=%s\r\n",ip_ntoa(addr));
    }
 	return result;
}

/*
*清除ETH连接下http接收的数据及长度
*/
void clear_eth_ota_recv_data(void)
{
	eth_http_parm.recvbuf_pos=0;
	memset(eth_http_parm.recv_buf,0,ETH_HTTP_RX_BUFSIZE); 
}

/*
*通过ETH向http server发送数据
*/
bool eth_http_send(char* str)
{
	if(tcp_http_conn!=NULL)
	{
		 eth_http_parm.send_buf=str;
		 eth_http_parm.send_flag |= LWIP_SEND_DATA;
		 return true;
		//err = netconn_write(tcp_clientconn ,str,strlen((char*)str),NETCONN_COPY); //发送数据
	}
	else
	{
		return false;
	}

}

extern osThreadId_t task_http_download_handle;

/*
*tcp  ota task
*/
struct netbuf *http_recvbuf;

void eth_ota_thread(void *arg)
{
	struct pbuf *q;
	err_t err,recv_err;

	static ip_addr_t loca_ipaddr;
	static u16_t 		 loca_port;
		
	LWIP_UNUSED_ARG(arg);
	
	while (1) 
	{
		LOG("connect to http server:%s\r\n",ip_ntoa(&ota_info.ip));
		tcp_http_conn=netconn_new(NETCONN_TCP);  //创建一个TCP链接
        err = netconn_connect(tcp_http_conn,&ota_info.ip,ota_info.port);//连接服务器
		if(err != ERR_OK)
		{
			netconn_delete(tcp_http_conn); //返回值不等于ERR_OK,删除tcp_clientconn连接
		}
		else if (err == ERR_OK)    //处理新连接的数据
		{ 
			
			tcp_http_conn->recv_timeout = 10;
			netconn_getaddr(tcp_http_conn,&loca_ipaddr,&loca_port,1); //获取本地IP主机IP地址和端口号
			LOG("connect server %s successful,local port:%d\r\n",
			ip_ntoa(&ota_info.ip),loca_port);
			//连接服务器成功后开启download任务
			osThreadState_t os_state = osThreadGetState(task_http_download_handle);
			LOG("http_download_task_state=%d\r\n",os_state);
			if(os_state!=osThreadError)
			{
				LOG("resume http task\r\n");
				//taskENTER_CRITICAL();
				osThreadResume(task_http_download_handle);
				//taskEXIT_CRITICAL();
			}
			else
			{
				LOG("create http task\r\n");
				task_http_download_handle=task_http_download_create();
			}
			while(1)
			{
				delay_ms(10);
				if(eth_http_parm.send_flag == true) //有数据要发送
				{
					err = netconn_write(tcp_http_conn ,
					eth_http_parm.send_buf,strlen((char*)eth_http_parm.send_buf),NETCONN_COPY); //发送tcp数据
					if(err != ERR_OK)
					{
						LOG("tcp send data fail!");
					}
					eth_http_parm.send_flag =false;
				}
					
				if((recv_err = netconn_recv(tcp_http_conn,&http_recvbuf)) == ERR_OK)  //接收到数据
				{	
					taskENTER_CRITICAL();
					/*if(eth_http_parm.recv_flag==false)//新数据进来，tcp_recv_flag为true时旧数据未处理，数据会叠加。
					{
						eth_http_parm.recvbuf_pos=0;
						memset(eth_http_parm.recv_buf,0,ETH_HTTP_RX_BUFSIZE);  //数据接收缓冲区清零
					}*/
					for(q=http_recvbuf->p;q!=NULL;q=q->next)  //遍历完整个pbuf链表
					{
						//判断要拷贝到TCP_CLIENT_RX_BUFSIZE中的数据是否大于TCP_CLIENT_RX_BUFSIZE的剩余空间，如果大于
						//的话就只拷贝TCP_CLIENT_RX_BUFSIZE中剩余长度的数据，否则的话就拷贝所有的数据
						if(q->len > (ETH_HTTP_RX_BUFSIZE-eth_http_parm.recvbuf_pos)) 
							memcpy(eth_http_parm.recv_buf+eth_http_parm.recvbuf_pos,q->payload,(ETH_HTTP_RX_BUFSIZE-tcp_parm.recvbuf_pos));//拷贝数据
						else 
							memcpy(eth_http_parm.recv_buf+eth_http_parm.recvbuf_pos,q->payload,q->len);
						eth_http_parm.recvbuf_pos += q->len;  	
						if(eth_http_parm.recvbuf_pos > TCP_CLIENT_RX_BUFSIZE) break; //超出TCP客户端接收数组,跳出	
					}
					
					//eth_http_parm.recv_flag=true;
					taskEXIT_CRITICAL();
					//osSemaphoreRelease (tcp_recv_semhandle);
					netbuf_delete(http_recvbuf);
				}
				else if(recv_err == ERR_CLSD)  //关闭连接
				{
					netconn_close(tcp_http_conn);
					netconn_delete(tcp_http_conn);
					LOG("Server %s disconnect\r\n",ip_ntoa(&ota_info.ip));
					break;
				}
				//LOG("recv_err=%d \r\n",recv_err);
			}
		}
	}
}

void delete_eth_ota_thread(void)
{
	netconn_close(tcp_http_conn);

	netconn_delete(tcp_http_conn);
	//osThreadSuspend(task_eth_ota_handle);
	osThreadTerminate(task_eth_ota_handle);
}





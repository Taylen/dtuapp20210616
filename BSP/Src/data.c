/*
 * File      : data.c
 * Change Logs:
 * Date           Author       Notes
 * 2021-05-30     zeng     first version
 */


#include "data.h"
#include "usart.h"
#include "w25qxx.h"


enum USE_CLASSES_FLAG	dtu_rtos_classes=USE_CLASSES_OFF;
SYSTEM_PARM system_parm;


//更新系统参数
void sys_parm_refresh(void)
{	
	uint8_t para_size=sizeof( SYSTEM_PARM);

	memset(&system_parm,0,para_size);

	strcpy((char *)&system_parm.soft_ver[0],(const char*)version_string);
	memset((char *)&system_parm.update_flag[0],FLAG_NORMAL,4);
	BSP_W25Qx_Write((uint8_t *)&system_parm,SYS_CONFIG_ADDR,para_size);

}

/*
*初始化系统参数
*/
void nv_config_init(void)
{
 	uint8_t para_size=sizeof( SYSTEM_PARM);
	
	uint32_t update_flag;

	BSP_W25Qx_Read((uint8_t *)&system_parm,SYS_CONFIG_ADDR, para_size);		//读系统配置数据
	
	update_flag = (system_parm.update_flag[0]<<24)+(system_parm.update_flag[1]<<16)+
	(system_parm.update_flag[2]<<8)+(system_parm.update_flag[3]);
	if(update_flag==FLAG_UPDATE_SUCCESS)
	{
		LOG("update fw success!\r\n");
	}
	else if(update_flag==FLAG_UPDATE_FAIL)
	{
		LOG("update fw fail!\r\n");
	}
	
	if(system_parm.soft_ver[1] !='.')	//首次烧录状态/格式不对
	{
		LOG("system parameter init\r\n");
	}
	sys_parm_refresh();

}




﻿/*
 * File      : w25qxx.c
 * Change Logs:
 * Date           Author       Notes
 * 2021-05-30     zeng     first version
 * 2021-06-15     Taylen   modify write
 */


 #include "w25qxx.h"
 #include "spi.h"
 #include "stdio.h"
 
 #define hspiflash hspi3
 
 
/**
  * @brief  Initializes the W25Q128FV interface.
  * @retval None
  */
uint8_t bsp_w25qx_init(void)
{ 
	uint8_t temp_id[4];
	
	/* Reset W25Qxxx */
	bsp_w25qx_reset();	
	bsp_w25qx_get_status();
	
	bsp_w25qx_read_id(temp_id);
	if(temp_id[0]==0xef&&temp_id[1]==0x17)
	{
		printf("FLASH:w25q128 read ok\r\n");
	}
	else
	{
		printf("FLASH:read error\r\n");
	} 
//	bsp_w25qx_Erase_Block(0);	
	return 0;
}

/**
  * @brief  This function reset the W25Qx.
  * @retval None
  */
static void	bsp_w25qx_reset(void)
{
	uint8_t cmd[2] = {RESET_ENABLE_CMD,RESET_MEMORY_CMD};
	
	W25Qx_Enable();
	/* Send the reset command */
	HAL_SPI_Transmit(&hspiflash, cmd, 2, W25Qx_TIMEOUT_VALUE);	
	W25Qx_Disable();

}

/**
  * @brief  Reads current status of the W25Q128FV.
  * @retval W25Q128FV memory status
  */
static uint8_t bsp_w25qx_get_status(void)
{
	uint8_t cmd[] = {READ_STATUS_REG1_CMD};
	uint8_t status;
	
	W25Qx_Enable();
	/* Send the read status command */
	HAL_SPI_Transmit(&hspiflash, cmd, 1, W25Qx_TIMEOUT_VALUE);	
	/* Reception of the data */
	HAL_SPI_Receive(&hspiflash,&status, 1, W25Qx_TIMEOUT_VALUE);
	W25Qx_Disable();
	
	/* Check the value of the register */
  if((status & W25Q128FV_FSR_BUSY) != 0)
  {
    return W25Qx_BUSY;
  }
	else
	{
		return W25Qx_OK;
	}		
}
/*
*等待flash空闲
*timeout:等待超时时间
*/
static uint8_t bsp_w25qx_wait_busy(uint32_t timeout)   
{   
	uint32_t tickstart = HAL_GetTick();
	
	while(bsp_w25qx_get_status() == W25Qx_BUSY)
	{
		/* Check for the Timeout */
	    if((HAL_GetTick() - tickstart) > timeout)
	    {        
			return W25Qx_TIMEOUT;
	    }
	}
	return W25Qx_OK;
}  

/**
  * @brief  This function send a Write Enable and wait it is effective.
  * @retval None
  */
uint8_t bsp_w25qx_write_enable(void)
{
	uint8_t cmd[] = {WRITE_ENABLE_CMD};
	uint32_t tickstart = HAL_GetTick();

	/*Select the FLASH: Chip Select low */
	W25Qx_Enable();
	/* Send the read ID command */
	HAL_SPI_Transmit(&hspiflash, cmd, 1, W25Qx_TIMEOUT_VALUE);	
	/*Deselect the FLASH: Chip Select high */
	W25Qx_Disable();
	
	/* Wait the end of Flash writing */
	while(bsp_w25qx_get_status() == W25Qx_BUSY)
	{
		/* Check for the Timeout */
    if((HAL_GetTick() - tickstart) > W25Qx_TIMEOUT_VALUE)
    {        
			return W25Qx_TIMEOUT;
    }
	}
	
	return W25Qx_OK;
}

/**
  * @brief  Read Manufacture/Device ID.
	* @param  return value address
  * @retval None
  */
void bsp_w25qx_read_id(uint8_t *ID)
{
	uint8_t cmd[4] = {READ_ID_CMD,0x00,0x00,0x00};
	
	W25Qx_Enable();
	/* Send the read ID command */
	HAL_SPI_Transmit(&hspiflash, cmd, 4, W25Qx_TIMEOUT_VALUE);	
	/* Reception of the data */
	HAL_SPI_Receive(&hspiflash,ID, 2, W25Qx_TIMEOUT_VALUE);
	W25Qx_Disable();
		
}

/**
  * @brief  Reads an amount of data from the QSPI memory.
  * @param  pData: Pointer to data to be read
  * @param  ReadAddr: Read start address
  * @param  Size: Size of data to read    
  * @retval QSPI memory status
  */
uint8_t bsp_w25qx_read(uint8_t* pData, uint32_t ReadAddr, uint32_t Size)
{
	uint8_t cmd[4];

	/* Configure the command */
	cmd[0] = READ_CMD;
	cmd[1] = (uint8_t)(ReadAddr >> 16);
	cmd[2] = (uint8_t)(ReadAddr >> 8);
	cmd[3] = (uint8_t)(ReadAddr);
	
	W25Qx_Enable();
	/* Send the read ID command */
	HAL_SPI_Transmit(&hspiflash, cmd, 4, W25Qx_TIMEOUT_VALUE);	
	/* Reception of the data */
	if (HAL_SPI_Receive(&hspiflash, pData,Size,W25Qx_TIMEOUT_VALUE) != HAL_OK)
  	{
    	return W25Qx_ERROR;
  	}
	W25Qx_Disable();
	return W25Qx_OK;
}

/**
  * @brief  Erases the specified block of the QSPI memory. 
  * @param  BlockAddress: Block address to erase  
  * @retval QSPI memory status
  */
uint8_t bsp_w25qx_erase_block(uint32_t Address)
{
	uint8_t cmd[4];
	uint32_t tickstart = HAL_GetTick();
	cmd[0] = SECTOR_ERASE_CMD;
	cmd[1] = (uint8_t)(Address >> 16);
	cmd[2] = (uint8_t)(Address >> 8);
	cmd[3] = (uint8_t)(Address);
	
	/* Enable write operations */
	bsp_w25qx_write_enable();
	
	/*Select the FLASH: Chip Select low */
	W25Qx_Enable();
	/* Send the read ID command */
	HAL_SPI_Transmit(&hspiflash, cmd, 4, W25Qx_TIMEOUT_VALUE);	
	/*Deselect the FLASH: Chip Select high */
	W25Qx_Disable();
	
	/* Wait the end of Flash writing */
	return bsp_w25qx_wait_busy(W25Q128FV_SECTOR_ERASE_MAX_TIME);

}

/**
  * @brief  Erases the specified  sector of the QSPI memory. 
  * @param  BlockAddress:  Sector address to erase  
  * @retval QSPI memory status
  */
uint8_t bsp_w25qx_erase_sector(uint32_t Address)
{
	uint8_t cmd[4];
	uint32_t tickstart = HAL_GetTick();
	cmd[0] = SECTOR_ERASE_CMD;
	cmd[1] = (uint8_t)(Address >> 16);
	cmd[2] = (uint8_t)(Address >> 8);
	cmd[3] = (uint8_t)(Address);
	
	/* Enable write operations */
	bsp_w25qx_write_enable();
	
	/*Select the FLASH: Chip Select low */
	W25Qx_Enable();
	/* Send the read ID command */
	HAL_SPI_Transmit(&hspiflash, cmd, 4, W25Qx_TIMEOUT_VALUE);	
	/*Deselect the FLASH: Chip Select high */
	W25Qx_Disable();
	
	/* Wait the end of Flash writing */
	return bsp_w25qx_wait_busy(W25Q128FV_SECTOR_ERASE_MAX_TIME);
}


/**
  * @brief  Erases the entire QSPI memory.This function will take a very long time.
  * @retval QSPI memory status
  */
uint8_t bsp_w25qx_erase_chip(void)
{
	uint8_t cmd[4];
	uint32_t tickstart = HAL_GetTick();
	cmd[0] = CHIP_ERASE_CMD;
	
	/* Enable write operations */
	bsp_w25qx_write_enable();
	
	/*Select the FLASH: Chip Select low */
	W25Qx_Enable();
	/* Send the read ID command */
	HAL_SPI_Transmit(&hspiflash, cmd, 1, W25Qx_TIMEOUT_VALUE);	
	/*Deselect the FLASH: Chip Select high */
	W25Qx_Disable();
	
	/* Wait the end of Flash writing */
	return bsp_w25qx_wait_busy(W25Q128FV_BULK_ERASE_MAX_TIME);
}
/*
*SPI在一页(0~65535)内写入少于256个字节的数据
//在指定地址开始写入最大256字节的数据
//pBuffer:数据存储区
//WriteAddr:开始写入的地址(24bit)
//NumByteToWrite:要写入的字节数(最大256),该数不应该超过该页的剩余字节数!!!	
*/
uint8_t bsp_w25qx_write_page(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t NumByteToWrite)
{
	uint8_t cmd[4];

	cmd[0] = PAGE_PROG_CMD;
	cmd[1] = (uint8_t)(WriteAddr >> 16);
	cmd[2] = (uint8_t)(WriteAddr >> 8);
	cmd[3] = (uint8_t)(WriteAddr);
	/* Enable write operations */
	bsp_w25qx_write_enable();
	
	W25Qx_Enable();
		
    /* Send the command */
    if (HAL_SPI_Transmit(&hspiflash,cmd, 4, W25Qx_TIMEOUT_VALUE) != HAL_OK)
    {
      	return W25Qx_ERROR;
    }
    
    /* Transmission of the data */
    if (HAL_SPI_Transmit(&hspiflash, pBuffer,NumByteToWrite, W25Qx_TIMEOUT_VALUE) != HAL_OK)
    {
      	return W25Qx_ERROR;
    }
	W25Qx_Disable();
	/* Wait the end of Flash writing */
	return bsp_w25qx_wait_busy(W25Qx_TIMEOUT_VALUE);
	
} 


/*无检验写SPI FLASH 
//必须确保所写的地址范围内的数据全部为0XFF,否则在非0XFF处写入的数据将失败!
//具有自动换页功能 
//在指定地址开始写入指定长度的数据,但是要确保地址不越界!
//pBuffer:数据存储区
//WriteAddr:开始写入的地址(24bit)
//NumByteToWrite:要写入的字节数(最大65535)
//CHECK OK
*/
void bsp_w25qx_write_nocheck(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t NumByteToWrite)   
{ 			 		 
	uint16_t pageremain;	   
	pageremain=256-WriteAddr%256; //单页剩余的字节数		 	    
	if(NumByteToWrite<=pageremain)pageremain=NumByteToWrite;//不大于256个字节
	while(1)
	{	   
		bsp_w25qx_write_page(pBuffer,WriteAddr,pageremain);
		if(NumByteToWrite==pageremain)break;//写入结束了
	 	else //NumByteToWrite>pageremain
		{
			pBuffer+=pageremain;
			WriteAddr+=pageremain;	

			NumByteToWrite-=pageremain;			  //减去已经写入了的字节数
			if(NumByteToWrite>256)pageremain=256; //一次可以写入256个字节
			else pageremain=NumByteToWrite; 	  //不够256个字节了
		}
	};	    
} 


/*写SPI FLASH  
*在指定地址开始写入指定长度的数据,该函数带擦除操作!
*pBuffer:数据存储区
*WriteAddr:开始写入的地址(24bit)						
*NumByteToWrite:要写入的字节数(最大65535)   
*/
void bsp_w25qx_write(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t NumByteToWrite)   
{ 
	uint32_t secpos;
	uint16_t secoff;
	uint16_t secremain;	   
 	uint16_t i;   
#ifdef SYSTEM_SUPPORT_OS 
	uint8_t * W25QXX_BUF;	  
	
	W25QXX_BUF=pvPortMalloc(W25Q128FV_SUBSECTOR_SIZE);
	if(W25QXX_BUF==NULL)
	{
		LOG("MALLOC ERROR\r\n");
		return ;
	}
#else
	uint8_t   W25QXX_BUF[W25Q128FV_SUBSECTOR_SIZE];

#endif
 	secpos=WriteAddr/W25Q128FV_SUBSECTOR_SIZE;//扇区地址  
	secoff=WriteAddr%W25Q128FV_SUBSECTOR_SIZE;//在扇区内的偏移
	secremain=W25Q128FV_SUBSECTOR_SIZE-secoff;//扇区剩余空间大小   
 	//printf("ad:%X,nb:%X\r\n",WriteAddr,NumByteToWrite);//测试用
 	if(NumByteToWrite<=secremain)secremain=NumByteToWrite;//不大于4096个字节
	while(1) 
	{	
		bsp_w25qx_read(W25QXX_BUF,secpos*W25Q128FV_SUBSECTOR_SIZE,W25Q128FV_SUBSECTOR_SIZE);//读出整个扇区的内容
		for(i=0;i<secremain;i++)//校验数据
		{
			if(W25QXX_BUF[secoff+i]!=0XFF)break;//需要擦除  	  
		}
		if(i<secremain)//需要擦除
		{
			bsp_w25qx_erase_sector(secpos*W25Q128FV_SUBSECTOR_SIZE);//擦除这个扇区
			for(i=0;i<secremain;i++)	   //复制
			{
				W25QXX_BUF[i+secoff]=pBuffer[i];	  
			}
			bsp_w25qx_write_nocheck(W25QXX_BUF,secpos*W25Q128FV_SUBSECTOR_SIZE,W25Q128FV_SUBSECTOR_SIZE);//写入整个扇区  

		}else bsp_w25qx_write_nocheck(pBuffer,WriteAddr,secremain);//写已经擦除了的,直接写入扇区剩余区间. 				   

		if(NumByteToWrite==secremain)
		{
#ifdef SYSTEM_SUPPORT_OS 
			vPortFree(W25QXX_BUF);
#endif
			break;//写入结束了
		}
		else//写入未结束
		{
			secpos++;//扇区地址增1
			secoff=0;//偏移位置为0 	 

		   	pBuffer+=secremain;  //指针偏移
			WriteAddr+=secremain;//写地址偏移	   
		   	NumByteToWrite-=secremain;				//字节数递减
			if(NumByteToWrite>W25Q128FV_SUBSECTOR_SIZE)secremain=W25Q128FV_SUBSECTOR_SIZE;	//下一个扇区还是写不完
			else secremain=NumByteToWrite;			//下一个扇区可以写完了
		}	 
	};	 
}




 /*
  * 函数功能: W25QXX 读写一个字节
  * 输入参数: TxData:要写入的字节
  * 返 回 值: 读取到的字节
  * 说    明：
  */
//uint8_t W25QXX_ReadWriteByte(uint8_t txdata)
//{
//    uint8_t Rxdata;
//    HAL_SPI_TransmitReceive(&hspiflash,&txdata,&Rxdata,1, 1000);       
//		return Rxdata;          		    //返回收到的数据		
//}

 
 
 




#ifndef __APP_WIFI_H
#define __APP_WIFI_H

#include "sys.h"

#define PIN_WIFI_POWER 		GPIOE,GPIO_Pin_7  //WIFI电源，低有效
#define PIN_WIFI_RESET 		(GPIOE,GPIO_Pin_8)  //低电平复位，>300ms
#define PIN_WIFI_NRELOAD	GPIOE,GPIO_Pin_9	//拉高3秒后恢复出厂
#define PIN_WIFI_LED		(GPIOE,GPIO_Pin_14)
#define PIN_WIFI_READY		(GPIOE,GPIO_Pin_13) //状态引脚，0-WiFi启动完成，否则为1


#define WIFI_READY   		PEin(13)
#define WIFI_CONNECT  		PEin(14)

typedef struct
{
	u8 init_step;		//初始化步骤
	u8 init_type;		//1-初始化成功
	u8 tcp_connect;		//tcp 是否连接
	u8 net_work;		//wifi连接状态
	u8 error_cnt;
}WIFI_DEVICE_INFO;

enum WIFI_INIT_STEP
{
	STEP_INIT1=0,
	STEP_INIT2,
	STEP_STAMODE,
	STEP_WSSSID,
	STEP_WSKEY,
	STEP_TCP,
	STEP_REBOOT,
	
};


#define CMD_WIFI_REBOOT   "AT+Z\r\n"
#define CMD_WIFI_APMODE   "AT+WMODE=AP\r\n"
#define CMD_WIFI_INIT1    "+++"
#define CMD_WIFI_INIT2    "a"
#define CMD_WIFI_STAMODE   "AT+WMODE=STA\r\n"
#if 1
#define CMD_WIFI_WSSSID   	"AT+WSSSID=HBCKYF\r\n" 
//#define CMD_WIFI_WSSSID   	"AT+WSSSID=HBCK\r\n" 

#define CMD_WIFI_WSKEY   	"AT+WSKEY=WPA2PSK,AES,aa88888888\r\n"
//#define CMD_WIFI_WSKEY   	"AT+WSKEY=WPA2PSK,AES,HBCK888888\r\n"
#define CMD_WIFI_TCP   		"AT+NETP=TCP,Client,8080,192.168.0.165\r\n"

//#define CMD_WIFI_TCP   		"AT+NETP=TCP,Client,8080,192.168.1.203\r\n"
#else
#define CMD_WIFI_WSSSID   	"AT+WSSSID=ZL\r\n" 
#define CMD_WIFI_WSKEY   	"AT+WSKEY=WPA2PSK,AES,32133710\r\n"
#define CMD_WIFI_TCP   		"AT+NETP=TCP,Client,8080,192.168.43.2\r\n"

#endif

#define CMD_WIFI_TCPDIS 	"AT+TCPDIS\r\n"  //断开TCP连接
#define CMD_WIFI_TCPLK   	"AT+TCPLK\r\n" 	//查询TCP是否建立连接

#define CMD_WIFI_ENTM   "AT+ENTM\r\n" 		//进入透传


void GPIO_Wifi_Init(void);
void Wifi_Reload(void);
void Wifi_init_cmd(void);
void Wifi_Reboot_cmd(void);

void Task_wifi_creat(void);
void wifi_recv_handle(char *buff);


#endif



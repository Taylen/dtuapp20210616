﻿/*
 * File      : 全局变量定义  data.h
 * Change Logs:
 * Date           Author       Notes
 * 2021-05-30     zeng     first version
 */


#ifndef __DATA_H
#define __DATA_H
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"
#include "stm32f4xx.h"
#include "core_cm4.h"
#include "stm32f4xx_hal.h"
#include "stdbool.h"
#include "bsp_version.h"



//4G WIFI ETH
enum USE_CLASSES_FLAG
{
	USE_CLASSES_OFF=0,
	USE_CLASSES_4G_FLAG,
	USE_CLASSES_ETH_FLAG,
	USE_CLASSES_WIFI_FLAG,
};
extern enum USE_CLASSES_FLAG	dtu_rtos_classes;

typedef struct  //系统参数
{
	uint8_t update_flag[4];		//升级标志--0X55555555-升级成功,0x1a1a1a1a-进入升级,0xa5a5a5a5-升级失败。0xffffffff-初始状态
	uint8_t soft_ver[5];		//软件版本号
	uint32_t file_len;			//文件长度	
	uint8_t check_sumer[2];		//文件校验和
	uint8_t reserved[17];		//保留

}SYSTEM_PARM;

extern SYSTEM_PARM system_parm;
void nv_config_init(void);



#endif



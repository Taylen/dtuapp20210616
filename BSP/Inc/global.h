﻿/*
 * File      : 全局定义，宏开关
 * Change Logs:
 * Date           Author       Notes
 * 2021-05-30     zeng     first version
 */
#ifndef _GLB_H_
#define _GLB_H_


/*
//-------- <<< Use Configuration Wizard in Context Menu >>> ------------------
*/

/*
 * 产品型号
 */
/*--------------------- product model -------------------------------
//
// <h> product model
//		<i>	product model selection.
//
//		<o> product model 
//			<0=>15kVA
//			<1=> 21kVA
// </h>
*/
#define BICYCLE_TYPE  1

/* -------------------------- 控制台终端重定向 ----------------------------- */
//
// <h> SHELL_CONSOLE
//		<i> Console terminal redirection
//
//		<o> CONSOLE_TTY(UART4-485 or USART1-RS232)
//			
//			<1=> USART1(RS232)
//      <2=> USART2(485)
// </h>
#define SHELL_CONSOLE 	1





/*
 * 调试模式，输出调试信息等
 */
/*--------------------- APP_DEBUG -------------------------------
//
// <h> debug Function
//		<i>	Application Debug Mode Switch.
//
//		<o> debug 
//			<0=> Application Debug Mode Disable
//			<1=> Application Debug Mode Enable
// </h>
*/
#define APP_DEBUG  1


/*
 * 代码保护功能
 */
/*--------------------- code_security function ------------------------------
// <h> Code Security Function
//		<i>	Code Security check and enable function.
//
//		<o> CodeSecurity
//			<0=> Code Security Disable
//			<1=> Code Security Enable
//</h>
*/
#define CODE_SECURITY_FUNCTION	0





/*
 * 测试ISP下载升级功能的命令
 */
/*--------------------- ISP FUNCTION -----------------------
//
// <h> ISP test function
//		<i>	ISP relocate itself to NFC SRAM, and burn APP from SPI flash to IFLASH0(0x00080000).
//
//		<o> ISP
//			<0=> ISP test function Disable
//			<1=> ISP test function Enable
//</h>
*/
#define ISP_FUNCTION  0



/*
 * WDT导致复位的最大计数
 */
/*--------------------- WDT_RESET_MAX_COUNT -------------------------------
//
// <h> WDT reset MAX count
//		<i>	WDT Reset MAX count.
//		<o> MAX count (1-5) <1-5>
//</h>
*/
#define	WDT_RESET_MAX_COUNT		3

/*
 * 定义是否使用RTOS
*/
#define	 SYSTEM_SUPPORT_OS		1

/*
//-------- <<< end of configuration section >>> ------------------------------
*/

/* 系统升级标志   */
#define  FLAG_ENTER_UPDATE     		(0x1a1a1a1a)
#define	 FLAG_UPDATE_SUCCESS		(0x55555555)
#define	 FLAG_UPDATE_FAIL			(0xa5a5a5a5)
#define  FLAG_NORMAL    			(0x01010101)





/* 待下载的系统应用软件的存放在SPI flash中的地址, 256KB. [0x81000 .. 0xC1000)  */
#define	APP_CODE_ADDR	0x00000000UL   //FONT_KAI_ADDR
#define	APP_CODE_SIZE	0x00020000UL

/* ---- 余下252KB ----  [0x000C1000 .. 0x00100000) */
#define	REMAIN_ADDR		0x000C1000UL

/* 代码页存放地址 */
#define	CODE_PAGE_ADDR	REMAIN_ADDR
#define	CODE_PAGE_SIZE	218240	/* 代码页烧写文件数据大小 */


/*系统参数存放地址--外部flash*/
#define SYS_CONFIG_ADDR 0x100000UL //1MB

/* 用作烧写升级的小代码存放在SPI flash中的地址，需要搬移到合适的RAM中运行 */
#define	ISP_CODE_ADDR	0x000F7000UL
#define	ISP_CODE_SIZE	0x00001000UL

/* 最高地址部分用作注册ID号(16字节) */
#define	UNIQUE_ID_ADDR	0x000FFFE0UL
#define	UNIQUE_ID_SIZE	0x00000010UL

/* if the NFC is not used, the NFC SRAM can be used for a generate purpose by the application */
#define	NFC_SRAM_ADDR	0x20100000UL
#define	NFC_SRAM_SIZE	0x00001000UL


#define RS485_EN_RX  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_SET)
#define RS485_EN_TX	 HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_RESET)



/*
 * 软件注册保护标识
 */
#define	UNIQUE_ID_REGISTERED	(0x11111111UL)	// 已经注册
extern volatile unsigned int unique_id_register_flag;


/*
 * 记录开机时间(times())
 */
extern volatile unsigned long powerboot_time;


#endif


 /*
 * File      : webclient.h
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * zengxiaohui43@163.com
 * Change Logs:
 * Date           Author       Notes
 * 2021-05-30     zeng     first version
 */

#ifndef __WEBCLIENT_H__
#define __WEBCLIENT_H__

#include <main.h>
#include <string.h>
#include "lwip.h"
#include "lwip/dns.h"
#if defined(WEBCLIENT_USING_MBED_TLS) || defined(WEBCLIENT_USING_SAL_TLS)
#include <tls_client.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif   

typedef struct web_client_t web_client_t;

/* default receive or send timeout */
#define WEBCLIENT_DEFAULT_TIMEO        6

#ifdef WEBCLIENT_DEBUG
#define DBG_LEVEL          DBG_LOG            
#else
#define DBG_LEVEL          3    
#endif

#ifndef web_malloc
#define web_malloc                     pvPortMalloc
#endif

#ifndef web_calloc
#define web_calloc                     calloc
#endif

#ifndef web_realloc
#define web_realloc                    realloc
#endif

#ifndef web_free
#define web_free                       vPortFree
#endif


#define WEBCLIENT_SW_VERSION           "2.1.2"
#define WEBCLIENT_SW_VERSION_NUM       0x20102

#define WEBCLIENT_HEADER_BUFSZ         4096
#define WEBCLIENT_RESPONSE_BUFSZ       4096



/* DEBUG level */
#define DBG_ERROR           0
#define DBG_WARNING         1
#define DBG_INFO            2
#define DBG_LOG             3

#if (DBG_LEVEL >= DBG_LOG)
#define LOG_D     printf
#else
#define LOG_D(...)
#endif

#if (DBG_LEVEL >= DBG_INFO)
#define LOG_I     printf
#else
#define LOG_I(...)
#endif

#if (DBG_LEVEL >= DBG_WARNING)
#define LOG_W     printf
#else
#define LOG_W(...)
#endif

#if (DBG_LEVEL >= DBG_ERROR)
#define LOG_E     printf
#else
#define LOG_E(...)
#endif


#define MY_EOK                          0               /**< There is no error */
#define MY_ERROR                        1               /**< A generic error happens */
#define MY_ETIMEOUT                     2               /**< Timed out */
#define MY_EFULL                        3               /**< The resource is full */
#define MY_EEMPTY                       4               /**< The resource is empty */
#define MY_ENOMEM                       5               /**< No memory */
#define MY_ENOSYS                       6               /**< No system */
#define MY_EBUSY                        7               /**< Busy */
#define MY_EIO                          8               /**< IO error */
#define MY_EINTR                        9               /**< Interrupted system call */
#define MY_EINVAL                       10              /**< Invalid argument */

enum WEBCLIENT_STATUS
{
    WEBCLIENT_OK,
    WEBCLIENT_ERROR,
    WEBCLIENT_TIMEOUT,
    WEBCLIENT_NOMEM,
    WEBCLIENT_NOSOCKET,
    WEBCLIENT_NOBUFFER,
    WEBCLIENT_CONNECT_FAILED,
    WEBCLIENT_DISCONNECT,
    WEBCLIENT_FILE_ERROR,
};

enum WEBCLIENT_METHOD
{
    WEBCLIENT_USER_METHOD,
    WEBCLIENT_GET,
    WEBCLIENT_POST,
};

struct  webclient_header
{
    char buffer[WEBCLIENT_HEADER_BUFSZ];
    size_t length;                      /* content header buffer size */

    size_t size;                        /* maximum support header size */
};

struct webclient_session
{
    struct webclient_header *header;    /* webclient response header information */
    int socket;
    int resp_status;

    char *host;                         /* server host */
    char *req_url;                      /* HTTP request address*/

    int chunk_sz;
    int chunk_offset;

    int content_length;
    size_t content_remainder;           /* remainder of content length */

    int is_tls;                   /* HTTPS connect */
#ifdef WEBCLIENT_USING_MBED_TLS
    MbedTLSSession *tls_session;        /* mbedtls connect session */
#endif
};

///* create webclient session and set header response size */
//struct webclient_session *webclient_session_create(size_t header_sz);
//char *web_strdup(const char *s);
///* send HTTP GET request */
//int webclient_get(struct webclient_session *session, const char *URI);
//int webclient_get_position(struct webclient_session *session, const char *URI, int position);

///* send HTTP POST request */
//int webclient_post(struct webclient_session *session, const char *URI, const char *post_data);

///* close and release wenclient session */
//int webclient_close(struct webclient_session *session);

//int webclient_set_timeout(struct webclient_session *session, int millisecond);

///* send or receive data from server */
//int webclient_read(struct webclient_session *session, unsigned char *buffer, size_t size);
//int webclient_write(struct webclient_session *session, const unsigned char *buffer, size_t size);

///* webclient GET/POST header buffer operate by the header fields */
//int webclient_header_fields_add(struct webclient_session *session, const char *fmt, ...);
//const char *webclient_header_fields_get(struct webclient_session *session, const char *fields);

///* send HTTP POST/GET request, and get response data */
//int webclient_response(struct webclient_session *session, unsigned char **response);
//int webclient_request(const char *URI, const char *header, const char *post_data, unsigned char **response);
//int webclient_request_header_add(char **request_header, const char *fmt, ...);
//int webclient_resp_status_get(struct webclient_session *session);
//int webclient_content_length_get(struct webclient_session *session);

//#ifdef RT_USING_DFS
///* file related operations */
//int webclient_get_file(const char *URI, const char *filename);
//int webclient_post_file(const char *URI, const char *filename, const char *form_data);
//#endif
#include "lwip/apps/mqtt_opts.h"
#include "lwip/err.h"

/**
 * Output ring-buffer size, must be able to fit largest outgoing publish message topic+payloads
 */
#ifndef WEB_OUTPUT_RINGBUF_SIZE
#define WEB_OUTPUT_RINGBUF_SIZE 256
#endif

/**
 * Number of bytes in receive buffer, must be at least the size of the longest incoming topic + 8
 * If one wants to avoid fragmented incoming publish, set length to max incoming topic length + max payload length + 8
 */
#ifndef WEB_VAR_HEADER_BUFFER_LEN
#define WEB_VAR_HEADER_BUFFER_LEN 128
#endif

/**
 * Maximum number of pending subscribe, unsubscribe and publish requests to server .
 */
#ifndef WEB_REQ_MAX_IN_FLIGHT
#define WEB_REQ_MAX_IN_FLIGHT 4
#endif

/**
 * Seconds between each cyclic timer call.
 */
#ifndef WEB_CYCLIC_TIMER_INTERVAL
#define WEB_CYCLIC_TIMER_INTERVAL 5
#endif

/**
 * Publish, subscribe and unsubscribe request timeout in seconds.
 */
#ifndef WEB_REQ_TIMEOUT
#define WEB_REQ_TIMEOUT 30
#endif




/** @ingroup WEB
 * Default WEB port */
#define WEB_PORT 80

/*---------------------------------------------------------------------------------------------- */
/* Connection with server */

/**
 * @ingroup WEB
 * Client information and connection parameters */
struct web_connect_client_info_t {
  /** Client identifier, must be set by caller */
  const char *client_id;
  /** User name and password, set to NULL if not used */
  char* client_user;
	char* client_pass;
  /** keep alive time in seconds, 0 to disable keep alive functionality*/
  u16_t keep_alive;
  /** will topic, set to NULL if will is not to be used,
      will_msg, will_qos and will retain are then ignored */
  const char* will_topic;
  const char* will_msg;
  u8_t will_qos;
  u8_t will_retain;
};

/**
 * @ingroup WEB
 * Connection status codes */
typedef enum
{
  WEB_CONNECT_ACCEPTED                 = 0,
  WEB_CONNECT_REFUSED_PROTOCOL_VERSION = 1,
  WEB_CONNECT_REFUSED_IDENTIFIER       = 2,
  WEB_CONNECT_REFUSED_SERVER           = 3,
  WEB_CONNECT_REFUSED_USERNAME_PASS    = 4,
  WEB_CONNECT_REFUSED_NOT_AUTHORIZED_  = 5,
  WEB_CONNECT_DISCONNECTED             = 256,
  WEB_CONNECT_TIMEOUT                  = 257
} web_connection_status_t;

/**
 * @ingroup WEB
 * Function prototype for WEB connection status callback. Called when
 * client has connected to the server after initiating a WEB connection attempt by
 * calling mqtt_connect() or when connection is closed by server or an error
 *
 * @param client WEB client itself
 * @param arg Additional argument to pass to the callback function
 * @param status Connect result code or disconnection notification @see mqtt_connection_status_t
 *
 */
typedef void (*web_connection_cb_t)(web_client_t *client, void *arg, web_connection_status_t status);


/**
 * @ingroup WEB
 * Data callback flags */
enum {
  /** Flag set when last fragment of data arrives in data callback */
  WEB_DATA_FLAG_LAST = 1
};

/** 
 * @ingroup WEB
 * Function prototype for WEB incoming publish data callback function. Called when data
 * arrives to a subscribed topic @see mqtt_subscribe
 *
 * @param arg Additional argument to pass to the callback function
 * @param data User data, pointed object, data may not be referenced after callback return,
          NULL is passed when all publish data are delivered
 * @param len Length of publish data fragment
 * @param flags WEB_DATA_FLAG_LAST set when this call contains the last part of data from publish message
 *
 */
typedef void (*web_incoming_data_cb_t)(void *arg, const u8_t *data, u16_t len, u8_t flags);


/** 
 * @ingroup WEB
 * Function prototype for WEB incoming publish function. Called when an incoming publish
 * arrives to a subscribed topic @see mqtt_subscribe
 *
 * @param arg Additional argument to pass to the callback function
 * @param topic Zero terminated Topic text string, topic may not be referenced after callback return
 * @param tot_len Total length of publish data, if set to 0 (no publish payload) data callback will not be invoked
 */
typedef void (*web_incoming_publish_cb_t)(void *arg, const char *topic, u32_t tot_len);


/**
 * @ingroup WEB
 * Function prototype for WEB request callback. Called when a subscribe, unsubscribe
 * or publish request has completed
 * @param arg Pointer to user data supplied when invoking request
 * @param err ERR_OK on success
 *            ERR_TIMEOUT if no response was received within timeout,
 *            ERR_ABRT if (un)subscribe was denied
 */
typedef void (*web_request_cb_t)(void *arg, err_t err);


/**
 * Pending request item, binds application callback to pending server requests
 */
struct web_request_t
{
  /** Next item in list, NULL means this is the last in chain,
      next pointing at itself means request is unallocated */
  struct web_request_t *next;
  /** Callback to upper layer */
  web_request_cb_t cb;
  void *arg;
  /** WEB packet identifier */
  u16_t pkt_id;
  /** Expire time relative to element before this  */
  u16_t timeout_diff;
};

/** Ring buffer */
struct web_ringbuf_t {
  u16_t put;
  u16_t get;
  u8_t buf[WEB_OUTPUT_RINGBUF_SIZE];
};
/** WEB client */
struct web_client_t
{
	
		struct webclient_header *header;    /* webclient response header information */
    int socket;
    int resp_status;

    char host[200];                         /* server host */
    char req_url[100];                      /* HTTP request address*/
		uint8_t server_ip[4];										/* server host IP */
	
    int chunk_sz;
    int chunk_offset;

    int content_length;
    size_t content_remainder;           /* remainder of content length */

    int is_tls;                   /* HTTPS connect */
#ifdef WEBCLIENT_USING_MBED_TLS
    MbedTLSSession *tls_session;        /* mbedtls connect session */
#endif
	
	
  struct tcp_pcb *conn;
  /** Connection callback */
  void *connect_arg;
  web_connection_cb_t connect_cb;
  /** Pending requests to server */
  struct web_request_t *pend_req_queue;
  struct web_request_t req_list[WEB_REQ_MAX_IN_FLIGHT];
  void *inpub_arg;
	/** Connection state */
  u8_t conn_state;
  /** Incoming data callback */
  web_incoming_data_cb_t data_cb;
  web_incoming_publish_cb_t pub_cb;
  /** Input */
  u32_t msg_idx;
  u8_t rx_buffer[WEB_VAR_HEADER_BUFFER_LEN];
  /** Output ring-buffer */
  struct web_ringbuf_t output;
};



err_t web_client_post(web_client_t *client, char *url);
#ifdef  __cplusplus
    }
#endif

#endif

/*
 * File      : bsp_tcp_client.h
 * Comment: TCP连接的各种功能函数
 * Change Logs:
 * Date           Author       Notes
 * 2021-06-08     Taylen     first version
 */


#ifndef LWIP_CLIENT_APP_H
#define LWIP_CLIENT_APP_H

/* USER CODE BEGIN Includes */
#include "usart.h"

#include "lwip.h"
//#include "lwip/dns.h"
#include "api.h" 


#include "w25qxx.h"

#include "FreeRTOS.h"
#include "task.h"

#include "main.h"
#include "cmsis_os.h"
#include "global.h"
#include "bsp_version.h"

/* USER CODE END Includes */

#define ETH_HTTP_RX_BUFSIZE 5000
#define TCP_CLIENT_RX_BUFSIZE 1024

#define DEFAULT_PORT 8080
#define LWIP_SEND_DATA			0X80    //定义有数据发送
#define HEARTBEAT_TIME		3000  //每隔3秒发送一次心跳包
typedef struct 
{
	uint8_t remoteip[4];						//远端主机IP地址 
	uint16_t server_port;
	uint8_t recv_buf[TCP_CLIENT_RX_BUFSIZE];	//TCP客户端接收数据缓冲区
	bool recv_flag;			//接收数据标志位
	
	char *send_buf;		//TCP发送数据
	bool send_flag; 							//TCP客户端数据发送标志位
	uint16_t recvbuf_pos;		//接收到的数据长度
	

}TcpParameter;

typedef struct 
{
	uint8_t recv_buf[ETH_HTTP_RX_BUFSIZE];	//TCP客户端接收数据缓冲区
	bool recv_flag;			//接收数据标志位
	
	char *send_buf;		//TCP发送数据
	bool send_flag; 							//TCP客户端数据发送标志位
	uint16_t recvbuf_pos;		//接收到的数据长度

}HTTP_PARM;

extern TcpParameter tcp_parm;
extern HTTP_PARM eth_http_parm;

void tcp_client_thread(void *arg);
osThreadId_t tcp_client_task_create(void);
void tcp_recv_data_handle(uint8_t *buff);
err_t get_host_by_name(const char *hostname,ip_addr_t *addr);
void clear_eth_ota_recv_data(void);
bool eth_http_send(char* str);
void delete_eth_ota_thread(void);

#endif


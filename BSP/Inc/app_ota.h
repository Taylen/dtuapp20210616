/*
 * File      : app_ota.h
 * Comment: 实现OTA升级的函数接口
 * Change Logs:
 * Date           Author       Notes
 * 2021-06-09     Taylen     first version
 */

#ifndef __APP_OTA_H
#define __APP_OTA_H

//#include "stmflash.h"
#include "usart.h"
//lwip
//#include "lwip/api.h"
#include "bsp_tcp_client.h"
#include "global.h"

//flash
#include "w25qxx.h"

#define FW_DOWNLOAD_ADDR_EXT  		APP_CODE_ADDR  //下载固件存放地址外部flash 
#define success                     1
#define error                       0

#define SEND_BUFF_MAX_SIZE  200 //定义HTTP发送数据最大长度
#define HTTP_DEFAULT_PORT  80
#if 1
typedef  struct
{
	uint8_t 	enter_update_flag;
	uint32_t	flash_write_addr; //保存fw的地址
	uint32_t 	file_len;		//固件大小（字节）
	uint16_t 	sumer;			//累加校验和
	uint16_t 	sumer_flag;		//是否支持CRC校验
	uint16_t 	pack_num;
	uint8_t 	ROM_switch;

}UPDATE_INFO;
#endif
typedef struct
{
	char url[200];
	char host[100];
	ip_addr_t ip;
	char path[100];
	int port;
	char md5[32];		//MD5校验
	uint8_t ota_update_flag;
	int file_size;		//固件大小
} OTA_INFO;

extern OTA_INFO ota_info;
//char HTTP_Download_Range(OTA_INFO ota_info1,  u16 bytes_range);
//char WiFi_Download_FW(OTA_INFO ota_info1, u16 bytes_range);
bool ota_cmd_analysis(char* data_buff);
osThreadId_t task_http_download_create(void);

#endif


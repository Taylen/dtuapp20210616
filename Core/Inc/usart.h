﻿/**
  ******************************************************************************
  * @file    usart.h
  * @brief   This file contains all the function prototypes for
  *          the usart.c file
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USART_H__
#define __USART_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "global.h"
/* USER CODE BEGIN Includes */
//-------- <<< Use Configuration Wizard in Context Menu >>> ------------------
#include <stdio.h>
#include <string.h>

#define TRACE_ENABLE

#ifdef TRACE_ENABLE

#define LOG 		printf  
#else	
#define LOG(x)
#endif


#define UART6_RX_BUF_SIZE  600
#define UART_RX_BUF_SIZE  255
#define UART_TX_BUF_SIZE  UART_RX_BUF_SIZE


typedef struct 
{
		uint8_t   uart_rx_tmp_process;//接收临时缓冲	
		uint8_t   uart_rx_buf[UART_RX_BUF_SIZE];//接收缓冲
		uint8_t   uart_tx_buf[UART_TX_BUF_SIZE];//发送缓冲
	  uint8_t   uart_rx_count; //计数
	  uint8_t   uart_rec_complete;//填充标志位	
		uint8_t 	under_adress;//下发地址
		uint8_t   uart_cmd_flag;//下发命令标志位
} UART_RXTX_BUF;

extern UART_RXTX_BUF uart1_rxtx_buf;
extern UART_RXTX_BUF uart2_rxtx_buf;
extern UART_RXTX_BUF uart3_rxtx_buf;
/* USER CODE END Includes */

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;
extern UART_HandleTypeDef huart6;

/* USER CODE BEGIN Private defines */


/* USER CODE END Private defines */

void MX_USART1_UART_Init(void);
void MX_USART2_UART_Init(void);
void MX_USART3_UART_Init(void);
void MX_USART6_UART_Init(void);

/* USER CODE BEGIN Prototypes */
void usart1_send_str(uint8_t *str);

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif

#endif /* __USART_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

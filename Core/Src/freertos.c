/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"
#include "global.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usart.h"
#include "shell.h"
#include "stdio.h"
#include "lwip.h"
#include "lwip/dns.h"
#include "w25qxx.h"
#include "stdio.h"
#include "bsp_version.h"
#include "bsp_tcp_client.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#define DHCP_MAX_TIME  100  //dhcp自动获取IP次数
extern struct netif gnetif;
extern ip4_addr_t ipaddr;
extern ip4_addr_t netmask;
extern ip4_addr_t gw;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 2048 * 4
};
/* Definitions for myTask02 */
osThreadId_t myTask02Handle;
const osThreadAttr_t myTask02_attributes = {
  .name = "myTask02",
  .priority = (osPriority_t) osPriorityLow,
  .stack_size = 128 * 4
};
/* Definitions for myQueue01 */
osMessageQueueId_t myQueue01Handle;
const osMessageQueueAttr_t myQueue01_attributes = {
  .name = "myQueue01"
};
/* Definitions for myTimer01 */
osTimerId_t myTimer01Handle;
const osTimerAttr_t myTimer01_attributes = {
  .name = "myTimer01"
};
/* Definitions for myMutex01 */
osMutexId_t myMutex01Handle;
const osMutexAttr_t myMutex01_attributes = {
  .name = "myMutex01"
};
/* Definitions for myBinarySem01 */
osSemaphoreId_t myBinarySem01Handle;
const osSemaphoreAttr_t myBinarySem01_attributes = {
  .name = "myBinarySem01"
};

osSemaphoreId_t my_uart1semhandle;
const osSemaphoreAttr_t my_uart1sem_attributes = {
  .name = "my_uart1sem"
};
osSemaphoreId_t my_uart2semhandle;
const osSemaphoreAttr_t my_uart2sem_attributes = {
  .name = "my_uart2sem"
};

osSemaphoreId_t my_uart3semhandle;
const osSemaphoreAttr_t my_uart3sem_attributes = {
  .name = "my_uart3sem"
};

osSemaphoreId_t my_uart6semhandle;
const osSemaphoreAttr_t my_uart6sem_attributes = {
  .name = "my_uart6sem"
};
/* Definitions for myCountingSem01 */
osSemaphoreId_t myCountingSem01Handle;
const osSemaphoreAttr_t myCountingSem01_attributes = {
  .name = "myCountingSem01"
};
/* Definitions for myEvent01 */
osEventFlagsId_t myEvent01Handle;
const osEventFlagsAttr_t myEvent01_attributes = {
  .name = "myEvent01"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

osSemaphoreId_t tcp_recv_semhandle;
const osSemaphoreAttr_t tcp_recv_attributes = {
  .name = "tcp_recvsem"
};



osThreadId_t task_led_handle;
const osThreadAttr_t task_led_attributes = {
  .name = "TaskLed",
  .priority = (osPriority_t) osPriorityLow2,
  .stack_size =128
};

//LED任务函数 
void led_task(void *argument)
{

    while(1)
    {
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
		osDelay(300);
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
		osDelay(300);
    }
} 

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void StartTask02(void *argument);
void Callback01(void *argument);

extern void MX_LWIP_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */


/*
*串口发送测试函数
*author:Taylen
*/
osThreadId_t task_usart1_send_handle;
const osThreadAttr_t task_usart1_send_attributes = {
  .name = "taskusart1Send",
  .priority = (osPriority_t) osPriorityLow3,
  .stack_size =300
};

//任务函数
#if 0
void usart1_send_task(void *argument)
{	
	uint32_t tick_last,tick_now;
	
    while(1)
    {
//		usart1_send_str("1\r\n");
//		osDelay(1000);
		tick_last=HAL_GetTick();
		osDelay(1000);
		tick_now=HAL_GetTick();
		//TRACE("now=%d,last=%d,TICK duration=%d\r\n",tick_now,tick_last,tick_now-tick_last);
		//osThreadTerminate(Task_usart1_send_Handle);
		
    }
} 
#endif
void  MX_PRINTF(void)
{
	printf("\033[2J");	// clear screen
	printf("\033[1;1H"); // 左上角，1行1列
	printf("/***************************************************************/\r\n");
	printf("                         DTU APP (%s)\r\n",version_string);
	printf("                         www.huabangit.com\r\n");
	printf("/***************************************************************/\r\n");
	printf("authon by zeng\r\n");
	LOG("system total heap size = %d\r\n", xPortGetFreeHeapSize());
}
/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */
  /* Create the mutex(es) */
  /* creation of myMutex01 */
  myMutex01Handle = osMutexNew(&myMutex01_attributes);

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* creation of myBinarySem01 */
  myBinarySem01Handle = osSemaphoreNew(1, 1, &myBinarySem01_attributes);

  /* creation of myCountingSem01 */
  myCountingSem01Handle = osSemaphoreNew(2, 2, &myCountingSem01_attributes);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
	my_uart1semhandle = osSemaphoreNew(1, 1, &my_uart1sem_attributes);
	my_uart2semhandle = osSemaphoreNew(1, 1, &my_uart2sem_attributes);
	my_uart3semhandle = osSemaphoreNew(1, 1, &my_uart3sem_attributes);
	my_uart6semhandle = osSemaphoreNew(1, 1, &my_uart6sem_attributes);

	tcp_recv_semhandle = osSemaphoreNew(1, 1, &tcp_recv_attributes);
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* creation of myTimer01 */
  myTimer01Handle = osTimerNew(Callback01, osTimerPeriodic, NULL, &myTimer01_attributes);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of myQueue01 */
  myQueue01Handle = osMessageQueueNew (16, sizeof(uint16_t), &myQueue01_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of myTask02 */
  myTask02Handle = osThreadNew(StartTask02, NULL, &myTask02_attributes);
  


  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* creation of myEvent01 */
  myEvent01Handle = osEventFlagsNew(&myEvent01_attributes);

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
    task_led_handle = osThreadNew(led_task, NULL, &task_led_attributes);
 
//  task_usart1_send_handle = osThreadNew(usart1_send_task, NULL, &task_usart1_send_attributes);
  /* USER CODE END RTOS_EVENTS */

}
extern int webclient_post_test(void);
extern int do_webclient_post(int argc, char **argv);


/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  	uint32_t over_time = 0;

#define  OBTAIN_IP_TIM  10
  /* USER CODE BEGIN StartDefaultTask */

	MX_LWIP_Init();
//		BSP_W25Qx_Init();
	__HAL_UART_ENABLE_IT(&huart1,UART_IT_IDLE);  //打开空闲中断
	HAL_UART_Receive_DMA(&huart1, uart1_rxtx_buf.uart_rx_buf, UART_RX_BUF_SIZE);		
	__HAL_UART_ENABLE_IT(&huart2,UART_IT_IDLE);  //打开空闲中断
	HAL_UART_Receive_DMA(&huart2, uart2_rxtx_buf.uart_rx_buf, UART_RX_BUF_SIZE);

	while(!dhcp_supplied_address(&gnetif))
    {
		over_time++;
        LOG("dhcp_connect: DHCP discovering... for %d times\r\n", over_time);
		
		if(over_time > DHCP_MAX_TIME)
		{
			LOG("dhcp_connect: overtime, not doing dhcp\r\n");
			break;
		}
        vTaskDelay(pdMS_TO_TICKS(500));
    }
    LOG("DHCP IP address: %s\r\n", ip4addr_ntoa((ip4_addr_t *)&gnetif.ip_addr.addr));
	LOG("DHCP mask: %s\r\n", ip4addr_ntoa((ip4_addr_t *)&gnetif.netmask.addr));
    LOG("DHCP gateway: %s\r\n", ip4addr_ntoa((ip4_addr_t *)&gnetif.gw.addr));
	dtu_rtos_classes=USE_CLASSES_ETH_FLAG;

    //创建TCP连接任务
    osThreadId_t tcp_client_handle;
	tcp_client_handle=tcp_client_task_create();
#if 0
	for(uint8_t i=0;i<20;i++)
	{
		printf("netif: ipaddr = %d.%d.%d.%d, netmask = %d.%d.%d.%d, gateway = %d.%d.%d.%d\r\n",
		gnetif.ip_addr.addr & 0x000000FF, (gnetif.ip_addr.addr >> 8) & 0x000000FF, (gnetif.ip_addr.addr >> 16) & 0x000000FF, (gnetif.ip_addr.addr >> 24) & 0x000000FF,
		gnetif.netmask.addr & 0x000000FF, (gnetif.netmask.addr >> 8) & 0x000000FF, (gnetif.netmask.addr >> 16) & 0x000000FF, (gnetif.netmask.addr >> 24) & 0x000000FF,
		gnetif.gw.addr & 0x000000FF, (gnetif.gw.addr >> 8) & 0x000000FF, (gnetif.gw.addr >> 16) & 0x000000FF, (gnetif.gw.addr >> 24) & 0x000000FF);		
		if(gnetif.ip_addr.addr!=0)
		{				
			break;
		}
		osDelay(3000);
	}
	#endif
		//webclient_post_test();
  /* Infinite loop */
  for(;;)
  {
	
    osDelay(1000);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartTask02 */
/**
* @brief Function implementing the myTask02 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask02 */
void StartTask02(void *argument)
{
  /* USER CODE BEGIN StartTask02 */
	
  /* Infinite loop */
	for(;;)
	{
		if(osSemaphoreAcquire (my_uart1semhandle,1)==osOK)
		{					
				
#if (CONSOLE_TTY == 1)
#endif
#if (CONSOLE_TTY == 2)

#endif	
			LOG("usart recv buff:%s",uart1_rxtx_buf.uart_rx_buf);
			for(uint8_t i=0;i<UART_RX_BUF_SIZE;i++)
			{
				uart1_rxtx_buf.uart_rx_buf[i]=0;
			}
			uart1_rxtx_buf.uart_rx_count=0;				
			HAL_UART_Receive_DMA(&huart1, uart1_rxtx_buf.uart_rx_buf, UART_RX_BUF_SIZE);			
		}
		if(osSemaphoreAcquire (my_uart2semhandle,1)==osOK)
		{	
			for(uint8_t i=0;i<UART_RX_BUF_SIZE;i++)
			{
				uart2_rxtx_buf.uart_rx_buf[i]=0;
			}
			uart2_rxtx_buf.uart_rx_count=0;							 
			HAL_UART_Receive_DMA(&huart2, uart2_rxtx_buf.uart_rx_buf, UART_RX_BUF_SIZE);			
		}
		if(osSemaphoreAcquire (tcp_recv_semhandle,1)==osOK)
		{
			tcp_recv_data_handle(tcp_parm.recv_buf);

		}
  	}
  /* USER CODE END StartTask02 */
}

/* Callback01 function */
void Callback01(void *argument)
{
  /* USER CODE BEGIN Callback01 */

  /* USER CODE END Callback01 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

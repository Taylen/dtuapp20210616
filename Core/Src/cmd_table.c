﻿/*
 * File      : 命令表
 * zengxiaohui43@163.com
 * Change Logs:
 * Date           Author       Notes
 * 2021-05-30     zeng     first version
 */

#include <string.h>
#include <stdio.h>

#include "shell.h"
#include "gpio.h"
#include "w25qxx.h"

const cmd_tbl_t * command_table_rtos[];
extern int do_webclient_post(int argc, char **argv);
const cmd_tbl_t cmd_webclient_post =
{
    "webclient_post",	3,		0,	do_webclient_post,
    "webclient_post - webclient_post test."
};

/*
 * 复位
 */
int do_reset(int argc, char * argv[])
{
    printf("\033[2J");	// clear screen
    printf("\033[1;1H"); // 回左上角，1行1列
    return(0);
}
const cmd_tbl_t cmd_reset =
{
    "reset",	1,		0,	do_reset,
    "reset - clear the terminal screen."
};
/*
 * 终端清屏
 */
int do_clear(int argc, char * argv[])
{
    printf("\033[2J");	// clear screen
    printf("\033[1;1H"); // 回左上角，1行1列
    return(0);
}
const cmd_tbl_t cmd_clear =
{
    "clear",	1,		0,	do_clear,
    "clear - clear the terminal screen."
};

/*
 *HELP
 */
int do_help(int argc, char *argv[])
{
    int i = 0, j;

    printf("\r\nNAME         USAGE\r\n");
		while(command_table_rtos[i] != NULL)
		{
				printf("%s", command_table_rtos[i]->name);

				for(j = 0; j < (13 - strlen(command_table_rtos[i]->name)); j++)
						printf(" ");
				printf("%s\r\n", command_table_rtos[i]->usage);
				i ++;
		}

    return(0);
}
/*
 * RTC
 */
int do_rtc(int argc, char * argv[])
{
	return 0;
}
const cmd_tbl_t cmd_rtc =
{
    "rtc", 8, 1, do_rtc,
    "rtc "
};
const cmd_tbl_t cmd_help = 
{
	"help",	1,		1,	do_help,
	"help - List all the commands."
};
const cmd_tbl_t cmd_hlp = 
{
	"?",	1,		1,	do_help,
	"? - List all the commands."
};



/*
 * 添加命令结构的指针链表，以NULL终结。
 */

const cmd_tbl_t * command_table_rtos[20] = 
{
	&cmd_rtc,
	&cmd_clear,
	&cmd_webclient_post,
	&cmd_reset,
	&cmd_help,
	&cmd_hlp,
	NULL
};



﻿ /*
 * File      : shell.c
 * zengxiaohui43@163.com
 * Change Logs:
 * Date           Author       Notes
 * 2021-05-30     zeng     first version
 */
 
 
 
#include <string.h>
#include <stdio.h>
#include "usart.h"
#include "shell.h"


static const char * sys_prompt_str_2 = "root@zengxiaohui#";

extern const cmd_tbl_t * command_table_rtos[];
extern int bootdelay;

// 选择命令表
const cmd_tbl_t ** pp_cmd_tbl_t;

/*
 * 查找命令结构列表的入口
 *
 * 返回值：
 *		找到匹配命令：返回命令结构指针；
 *		未找到：返回NULL。
 */
static const cmd_tbl_t * find_cmd(const char * cmd)
{
    const char * p;
    int i, len, n_found;

    const cmd_tbl_t * cmdtp;
    const cmd_tbl_t * cmdtp_temp;

    /*
     * Some commands allow length modifiers (like "cp.b");
     * compare command name only until first dot.
     */
    len = ((p = strchr(cmd, '.')) == NULL) ?	strlen(cmd) : (p - cmd);

    i = 0;
    n_found = 0;


    pp_cmd_tbl_t = command_table_rtos;
    while(pp_cmd_tbl_t[i] != NULL)
    {
        cmdtp = pp_cmd_tbl_t[i];

        if(strncmp(cmd, cmdtp->name, len) == 0)
        {
            if( len == strlen(cmdtp->name) ) // full match
                return(cmdtp);

            cmdtp_temp = cmdtp;	/* abbreviated command ? */
            n_found ++;
        }

        i ++;
    }

    if (n_found == 1) // exactly one match
        return(cmdtp_temp);

    return(NULL);	/* not found or ambiguous command */
}


/*
 * 仅支持退格编辑,以回车键结束输入。
 * 函数返回时可以确保line所指字符串带有终结符。
 *
 * 参数：
 *		line: 输入缓冲区起始地址；
 *		max_len: 输入缓冲区大小(包含终结符的一个字节)
 *
 * 返回值：
 *		>=0：输入的字符个数;
 *		-1: 控制台输入出错。
 */
static int get_line(char * line, int max_len)
{
    int rt, cnt;
    unsigned char c;

    cnt = 0;

    do
    {
        // read character.
        if( (rt = getchar()) == EOF ) // 阻塞
        {
            * line = 0;
            return(-1);
        }

        c = (unsigned char)rt;

        // process backspace.
        if( (c == VT_BACKSPACE) && (cnt > 0) )
        {
            cnt --;             // decrement count
            line --;            // line pointer

            printf("\033[1D");	// echo backspace
            putchar(' ');
            printf("\033[1D");
        }
        else
        {
            // 可显示的ASCII
            if( (c >= 0x20) && (c <= 0x7E) && (cnt < (max_len - 1)) )
            {
                // store and echo character
                cnt ++;
                *line ++ = c;
                putchar(c);
            }
        }
    } while(c != VT_CR);

    // mark end of string.
    * line = 0;

    return(cnt);
}

/*
 * 输入命令串的预处理
 * 函数返回时会在每个参数末尾加入终结符.
 *
 * 参数：
 *		line：输入命令串的起始地址；
 *		argv：各参数的地址列表。
 * 返回值：
 *		参数个数;
 *		0表示不是个命令。
 */
static int parse_line(char * line, char * argv[])
{
    int nargs = 0;

    while(nargs < CFG_MAXARGS)	// 输入参数个数包括命令本身在内
    {
        /* skip any white space */
        while ((*line == ' ') || (*line == '\t'))
            ++line;

        if(*line == '\0')	// end of line, no more args
        {
            argv[nargs] = NULL;
            return(nargs);
        }

        argv[nargs ++] = line;	// begin of argument string

        /* find end of string */
        while(*line && (*line != ' ') && (*line != '\t'))
            ++line;

        if (*line == '\0')	// end of line, no more args
        {
            argv[nargs] = NULL;
            return(nargs);
        }

        *line ++ = '\0';		// terminate current arg
    }

    //
    printf("\n** Too many args (max. %d) **\n", CFG_MAXARGS);
    return(nargs);
}



/*
 * 命令行前缀
 */
static void prefix_line(char * prompt)
{
    int i;

    putchar(0x0D);
    putchar(0x0A);

    for(i = 0; i < strlen(prompt); i++)
    {
        putchar(prompt[i]);
    }
}



/*
 * rtos_main_loop
 */
void rtos_main_loop(char *cmd_buf)
{
    int argc, rt;
    char * argv[CFG_MAXARGS + 1];	// terminated with NULL
    const cmd_tbl_t * cmdtp;
    int repeatable = 0;
    char  lastcommand[144]; // command input buffer
    {       
				rt=strlen(cmd_buf);
        if(rt == -1)
        {
            printf("\n** get_line() error !\n");
            return;
        }

        if(rt == 0) // 直接输入回车
        {
            if(repeatable == 1)
            {
                strcpy(cmd_buf, lastcommand);
                printf("%s", lastcommand);
            }
            else
                return;
        }
        else
            strcpy(lastcommand, cmd_buf);

        /* Extract arguments */
        if( (argc = parse_line(cmd_buf, argv)) == 0 ) //  Not a command at all !
        {
            repeatable = 0;
             return;
        }
				
        /* Look up command in command table */
        if( (cmdtp = find_cmd(argv[0])) == NULL )
        {
						printf("command error\r\n");
            repeatable = 0;
            printf("\nUnknown command '%s' - try 'help'\n", argv[0]);
             return;
        }

        /* found - check max args */
        if(argc > cmdtp->maxargs)
        {
            repeatable = 0;
            printf("\nUsage:\n%s\n", cmdtp->usage);
             return;
        }

        /* call function to do the command */
        if( (cmdtp->cmd)(argc, argv) == 0) // 命令执行正确
            repeatable = cmdtp->repeatable;
        else
        {
            repeatable = 0;
            printf("\r\n**command '%s' execute failure !\n", argv[0]);
        }
				prefix_line((char *)sys_prompt_str_2);
    }
}


